package com.dbabichev.timetable.model;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/27/2016
 * Time: 11:45 AM
 */
public class TimetableRow {

    public final Clazz clazz;
    public final TimetableWeek week;

    public TimetableRow(Clazz clazz, TimetableWeek week) {
        this.clazz = clazz;
        this.week = week;
    }
}
