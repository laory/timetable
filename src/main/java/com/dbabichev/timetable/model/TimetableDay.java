package com.dbabichev.timetable.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/27/2016
 * Time: 11:29 AM
 */
public class TimetableDay {

    public final List<TimetableCell> cells;

    public TimetableDay(List<TimetableCell> cells) {
        this.cells = Collections.unmodifiableList(cells);
    }

    @Override
    public String toString() {
        return "TimetableDay{" +
               "cells=" + cells +
               '}';
    }
}
