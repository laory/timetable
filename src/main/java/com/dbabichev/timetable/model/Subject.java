package com.dbabichev.timetable.model;

import com.dbabichev.timetable.json_util.ClassSerializer;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/19/2016
 * Time: 10:36 AM
 * <p>
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Subject {

    public final String                    id;
    public final String                    title;
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true) // otherwise first ref as POJO, others as id
    public final Teacher                   teacher;
    @JsonSerialize(keyUsing = ClassSerializer.class)
    public final Map<Clazz, HoursSettings> subjectLoad;

    public Subject(String id, String title, Teacher teacher, Map<Clazz, HoursSettings> subjectLoad) {
        this.id = id;
        this.title = title;
        this.teacher = teacher;
        this.subjectLoad = subjectLoad;
    }

    public Subject(String title, Teacher teacher, Map<Clazz, HoursSettings> subjectLoad) {
        this(null, title, teacher, subjectLoad);
    }

    public static <K, V> Map<K, V> transformSubjectLoad(Map<Clazz, HoursSettings> subjectLoad,
                                                        Function<Clazz, K> keyMapper,
                                                        Function<HoursSettings, V> valueMapper) {
        if (subjectLoad == null)
            return Collections.emptyMap();
        return subjectLoad.entrySet().stream().collect(Collectors.toMap(entry -> keyMapper.apply(entry.getKey()),
                                                                        entry -> valueMapper.apply(entry.getValue())));
    }

    public Map<String, Map<String, Object>> getPureSubjectLoad() {
        return transformSubjectLoad(subjectLoad, clazz -> clazz.id, HoursSettings:: toMap);
    }

    public Map<String, Integer> getPureHoursPerClass() {
        return transformSubjectLoad(subjectLoad, clazz -> clazz.id, HoursSettings:: getHours);
    }

    public <K, V> Map<K, V> transformSubjectLoad(Function<Clazz, K> keyMapper, Function<HoursSettings, V> valueMapper) {
        return transformSubjectLoad(subjectLoad, keyMapper, valueMapper);
    }

    @Override
    public String toString() {
        return "Subject{" + "id='" + id + '\'' + ", title='" + title + '\'' + ", teacher=" + teacher +
               ", subjectLoad=" + subjectLoad + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Subject subject = (Subject) o;

        if (id != null ? !id.equals(subject.id) : subject.id != null)
            return false;
        if (title != null ? !title.equals(subject.title) : subject.title != null)
            return false;
        if (teacher != null ? !teacher.equals(subject.teacher) : subject.teacher != null)
            return false;
        return subjectLoad != null ? subjectLoad.equals(subject.subjectLoad) : subject.subjectLoad == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (teacher != null ? teacher.hashCode() : 0);
        result = 31 * result + (subjectLoad != null ? subjectLoad.hashCode() : 0);
        return result;
    }
}
