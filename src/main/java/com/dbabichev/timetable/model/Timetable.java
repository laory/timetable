package com.dbabichev.timetable.model;

import com.dbabichev.timetable.json_util.ObjectMapperFactory;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/19/2016
 * Time: 10:36 AM
 * <p>
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Timetable {

    public final String                    id;
    public final Map<Clazz, TimetableWeek> table;

    public Timetable(String id, List<Clazz> classes, int workingDays, int lessons) {
        this(id, buildTable(classes, workingDays, lessons));
    }

    public Timetable(String id, Map<Clazz, TimetableWeek> table) {
        this.id = id;
        this.table = Collections.unmodifiableMap(table);
    }

    private static Map<Clazz, TimetableWeek> buildTable(List<Clazz> classes, int workingDays, int lessons) {
        return classes.stream().map(clazz -> {
            List<TimetableDay> week = IntStream.range(0, workingDays).mapToObj(day -> {
                List<TimetableCell> lessonsPerDay = IntStream.range(0, lessons).mapToObj(lesson -> new TimetableCell())
                                                             .collect(Collectors.toList());
                return new TimetableDay(lessonsPerDay);
            }).collect(Collectors.toList());
            return new TimetableRow(clazz, new TimetableWeek(week));
        }).collect(Collectors.toMap(row -> row.clazz, row -> row.week));
    }

    @Override
    public String toString() {
        return "Timetable{id='" + id + "\', table=" + table + '}';
    }

    public static void main(String[] args) throws JsonProcessingException {
        Timetable timetable = new Timetable("testId", Arrays.asList(new Clazz("class1Id", "class1"),
                                                                    new Clazz("class2Id", "class2")), 3, 5);
        TimetableWeek class1 = timetable.table.get(new Clazz("class1Id", "class1"));
        TimetableDay timetableDay = class1.timetableDays.get(0);
        timetableDay.cells.get(3).setSubject(new Subject("subject1Id", "subject1", null, null));

        System.out.println(ObjectMapperFactory.getTimetableObjectMapper().writeValueAsString(timetable));
    }
}
