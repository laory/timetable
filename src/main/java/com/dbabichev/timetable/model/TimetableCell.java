package com.dbabichev.timetable.model;

import com.fasterxml.jackson.annotation.*;

import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/27/2016
 * Time: 11:15 AM
 */
public class TimetableCell {

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true) // otherwise first ref as POJO, others as id
    private Subject subject;

    public TimetableCell(Subject subject) {
        this.subject = subject;
    }

    public TimetableCell() {
        this(null);
    }

    public Optional<Subject> getSubject() {
        return Optional.ofNullable(subject);
    }

    public void setSubject(Subject newSubject) {
        subject = newSubject;
    }

    @Override
    public String toString() {
        return "TimetableCell{" +
               "subject=" + subject +
               '}';
    }
}
