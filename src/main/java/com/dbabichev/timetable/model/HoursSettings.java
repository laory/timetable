package com.dbabichev.timetable.model;

import com.dbabichev.timetable.json_util.HoursSettingsSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 1/20/2017
 * Time: 10:38 AM
 */
@JsonSerialize(using = HoursSettingsSerializer.class)
public class HoursSettings {

    private final Map<String, Object> settings = new HashMap<>();

    public HoursSettings() {
    }

    public HoursSettings(Map<? extends String, ?> otherMap) {
        settings.putAll(otherMap);
    }

    public static final String HOURS     = "hours";
    public static final String DIVIDABLE = "dividable";

    public Boolean isDividable() {
        return (Boolean) settings.getOrDefault(DIVIDABLE, false);
    }

    public Boolean setDividable(boolean dividable) {
        return (Boolean) settings.put(DIVIDABLE, dividable);
    }

    public Integer getHours() {
        return (Integer) settings.get(HOURS);
    }

    public Integer setHours(int hours) {
        return (Integer) settings.put(HOURS, hours);
    }

    public Map<String, Object> toMap() {
        return new HashMap<>(settings);
    }

    public void putAll(HoursSettings hoursSettings) {
        settings.putAll(hoursSettings.toMap());
    }

    public void putAll(Map<?extends String, ?> otherMap) {
        settings.putAll(otherMap);
    }

    @Override
    public String toString() {
        return "HoursSettings{" + "settings=" + settings + '}';
    }
}
