package com.dbabichev.timetable.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/27/2016
 * Time: 11:27 AM
 */
public class TimetableWeek {

    public final List<TimetableDay> timetableDays;

    public TimetableWeek(List<TimetableDay> timetableDays) {
        this.timetableDays = Collections.unmodifiableList(timetableDays);
    }

    @Override
    public String toString() {
        return "TimetableWeek{" +
               "timetableDays=" + timetableDays +
               '}';
    }
}
