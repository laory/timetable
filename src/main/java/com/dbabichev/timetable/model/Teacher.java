package com.dbabichev.timetable.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/19/2016
 * Time: 10:36 AM
 * <p>
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Teacher {

    public final String  id;
    public final String  name;
    public final Boolean isManager;
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true) // otherwise first ref as POJO, others as id
    public final Clazz   managedClass;

    public Teacher(String id, String name, Boolean isManager, Clazz managedClass) {
        this.id = id;
        this.name = name;
        this.isManager = isManager;
        this.managedClass = managedClass;
    }

    public Teacher(String name, Boolean isManager, Clazz managedClass) {
        this.id = null;
        this.name = name;
        this.isManager = isManager;
        this.managedClass = managedClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Teacher teacher = (Teacher) o;

        if (id != null ? !id.equals(teacher.id) : teacher.id != null)
            return false;
        if (name != null ? !name.equals(teacher.name) : teacher.name != null)
            return false;
        if (isManager != null ? !isManager.equals(teacher.isManager) : teacher.isManager != null)
            return false;
        return managedClass != null ? managedClass.equals(teacher.managedClass) : teacher.managedClass == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (isManager != null ? isManager.hashCode() : 0);
        result = 31 * result + (managedClass != null ? managedClass.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Teacher{" +
               "id='" + id + '\'' +
               ", name='" + name + '\'' +
               ", isManager=" + isManager +
               ", managedClass=" + managedClass +
               '}';
    }
}
