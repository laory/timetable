package com.dbabichev.timetable.model;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 30/12/16
 * Time: 11:18 PM
 */
public class Cell {

    public final String clazz;
    public final int    day;
    public final int    lesson;
    public final String subject;
    public final int    division;

    public Cell(String clazz, int day, int lesson, String subject, int division) {
        this.clazz = clazz;
        this.day = day;
        this.lesson = lesson;
        this.subject = subject;
        this.division = division;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Cell cell = (Cell) o;

        if (day != cell.day)
            return false;
        if (lesson != cell.lesson)
            return false;
        if (division != cell.division)
            return false;
        if (clazz != null ? !clazz.equals(cell.clazz) : cell.clazz != null)
            return false;
        return subject != null ? subject.equals(cell.subject) : cell.subject == null;
    }

    @Override
    public int hashCode() {
        int result = clazz != null ? clazz.hashCode() : 0;
        result = 31 * result + day;
        result = 31 * result + lesson;
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + division;
        return result;
    }
}
