package com.dbabichev.timetable.web_socket.handler;

import com.fasterxml.jackson.databind.JsonNode;

import static com.dbabichev.timetable.web_socket.handler.Handler.ROUTING_KEY;
import static com.dbabichev.timetable.web_socket.handler.TimetableModificationHandler.TIMETABLE_MODIFICATION;

public class HandlerFactory {

    public static Handler getHandler(JsonNode message) {
        final String routingKey = message.get(ROUTING_KEY).asText();
        switch (routingKey) {
            case TIMETABLE_MODIFICATION:
                return new TimetableModificationHandler();
            default:
                throw new IllegalArgumentException("Routing key: \"" + routingKey + "\" has not been recognized");
        }
    }
}