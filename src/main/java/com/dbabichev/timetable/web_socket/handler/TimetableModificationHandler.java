package com.dbabichev.timetable.web_socket.handler;

import com.dbabichev.timetable.db.ConnectionProvider;
import com.dbabichev.timetable.db.dao.MongoTimetableDao;
import com.dbabichev.timetable.db.dao.TimetableDao;
import com.dbabichev.timetable.model.Cell;
import com.fasterxml.jackson.databind.JsonNode;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 31/12/16
 * Time: 1:01 AM
 */
public class TimetableModificationHandler implements Handler {

    public static final  String       TIMETABLE_MODIFICATION = "timetableModification";
    private final        TimetableDao timetableDao           =
            new MongoTimetableDao(ConnectionProvider.getConnection());
    private static final Logger       LOG                    =
            LoggerFactory.getLogger(TimetableModificationHandler.class);

    @Override
    public void handle(RemoteEndpoint remote, JsonNode message) {
        String operation = message.get(OPERATION).asText();
        switch (operation) {
            case "updateCell":
                updateCell(remote, message);
                break;
            case "deleteCell":
                deleteCell(remote, message);
                break;
        }
    }

    private void updateCell(RemoteEndpoint remote, JsonNode message) {
        final String classId = message.get("clazz").asText();
        final int day = message.get("day").asInt();
        final int lesson = message.get("lesson").asInt();
        final String subjectId = message.get("subject").asText();
        final int division = message.hasNonNull("division") ? message.get("division").asInt() : 0;
        Cell cell = new Cell(classId, day, lesson, subjectId, division);
        timetableDao.saveTimetableCell(cell).thenAccept(updatedCell -> {
            LOG.info("Timetable cell has been updated: {}", updatedCell);
            HashMap<String, Object> response = new HashMap<String, Object>(5) {{
                put("clazz", updatedCell.clazz);
                put("lesson", updatedCell.lesson);
                put("day", updatedCell.day);
                put("subject", updatedCell.subject);
                put("division", updatedCell.division);
            }};
            Handler.broadcast(TIMETABLE_MODIFICATION, "updateCell", response);
        });
    }

    private void deleteCell(RemoteEndpoint remote, JsonNode message) {
        final String classId = message.get("clazz").asText();
        final String subjectId = message.get("subject").asText();
        final int day = message.get("day").asInt();
        final int lesson = message.get("lesson").asInt();
        final int division = message.hasNonNull("division") ? message.get("division").asInt() : 0;
        Cell cell = new Cell(classId, day, lesson, subjectId, division);
        timetableDao.deleteTimetableCell(cell).thenAccept(isRemoved -> {
            if (isRemoved) {
                LOG.info("Timetable cell has been deleted: {}", cell);
                HashMap<String, Object> response = new HashMap<String, Object>(5) {{
                    put("clazz", classId);
                    put("lesson", lesson);
                    put("day", day);
                    put("subject", subjectId);
                    put("division", division);
                }};
                Handler.broadcast(TIMETABLE_MODIFICATION, "deleteCell", response);
            } else {
                LOG.info("Timetable cell has been deleted: {}. Removal was not acknowledged", cell);
            }
        });
    }
}
