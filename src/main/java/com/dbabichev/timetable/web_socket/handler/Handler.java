package com.dbabichev.timetable.web_socket.handler;

import com.dbabichev.timetable.web_socket.TimetableWebSocket;
import com.fasterxml.jackson.databind.JsonNode;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 31/12/16
 * Time: 1:02 AM
 */
public interface Handler {

    String OPERATION   = "operation";
    String ROUTING_KEY = "routingKey";

    void handle(RemoteEndpoint remote, JsonNode message);

    static void broadcast(String routingKey, String operation, Map<String, Object> data) {
        Map<String, Object> response = new HashMap<String, Object>(6) {{
            put(Handler.ROUTING_KEY, routingKey);
            put(Handler.OPERATION, operation);
            putAll(data);
        }};
        TimetableWebSocket.broadcast(response);
    }
}
