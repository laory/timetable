package com.dbabichev.timetable.web_socket;

import com.dbabichev.timetable.web_socket.handler.Handler;
import com.dbabichev.timetable.web_socket.handler.HandlerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 30/12/16
 * Time: 11:39 PM
 */
@WebSocket
public class TimetableWebSocket {

    public static final  ObjectMapper MAPPER = new ObjectMapper();
    private static final Logger       LOG    = LoggerFactory.getLogger(TimetableWebSocket.class);

    private static final Queue<Session> sessions = new ConcurrentLinkedQueue<>();

    @OnWebSocketConnect
    public void connected(Session session) {
        LOG.debug("Session: {} has been initialized", session);
        session.setIdleTimeout(-1);
        sessions.add(session);
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        LOG.debug("Session: {} has been shut down. Status: {}. Reason: {}", session, statusCode, reason);
        sessions.remove(session);
    }

    @OnWebSocketMessage
    public void message(Session session, String messageStr) {
        try {
            LOG.debug("Web socket message has been received: {}", messageStr);
            try {
                JsonNode message = MAPPER.readTree(messageStr);
                Handler handler = HandlerFactory.getHandler(message);
                handler.handle(session.getRemote(), message);
            } catch (Exception e) {
                LOG.error("Unable to handle web socket message: {}. Session: {}", messageStr, session, e);
                sendMessage(new HashMap<String, Object>() {{
                    put("status", 500);
                    put("message", "Unable to handle message. Reason: " + e.getMessage());
                    put("request", messageStr);
                }}, session);
            }
        } catch (Exception e) {
            LOG.error("Unable to respond: {}. Session: {}", messageStr, session, e);
        }
    }

    public static void broadcast(Map<String, Object> response) {
        sessions.forEach(session -> {
            sendMessage(response, session);
        });
    }

    public static void sendMessage(Map<String, Object> response, Session session) {
        try {
            session.getRemote().sendString(MAPPER.writeValueAsString(response));
        } catch (IOException e) {
            throw new BroadcastException("Unable to send a message. Session: " + session, e);
        }
    }

    public static class BroadcastException extends RuntimeException {

        public BroadcastException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}

