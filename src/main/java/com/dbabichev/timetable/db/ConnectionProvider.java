package com.dbabichev.timetable.db;

import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 11:07 AM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public final class ConnectionProvider {

    private static final String DB_NAME  = "db";
    public static final  String CELLS    = "cellsV2";
    public static final  String SUBJECTS = "subjectsV1";
    public static final  String TEACHERS = "teachers";
    public static final  String CLASSES  = "classes";

    private static final class ConnectionHolder {

        private static final MongoClient MONGO_CLIENT = MongoClients.create();
    }

    public static MongoDatabase getConnection() {
        return ConnectionHolder.MONGO_CLIENT.getDatabase(DB_NAME);
    }
}
