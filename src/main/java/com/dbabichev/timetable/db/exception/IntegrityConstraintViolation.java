package com.dbabichev.timetable.db.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

public class IntegrityConstraintViolation extends RuntimeException {

    private final String                    id;
    private final String                    property;
    private final static ObjectMapper MAPPER = new ObjectMapper();

    public IntegrityConstraintViolation(String id, String property, String message) {
        super(message);
        this.id = id;
        this.property = property;
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        try {
            return MAPPER.writeValueAsString(new HashMap<String, Object>() {{
                put("exception", IntegrityConstraintViolation.class.getSimpleName());
                put("foreignKey", id);
                put("property", property);
                put("message", message);
            }});
        } catch (JsonProcessingException e) {
            // ignore
        }
        return message;
    }

    @Override
    public String toString() {
        return "IntegrityConstraintViolation{" + "foreignKey='" + id + '}';
    }
}