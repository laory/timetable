package com.dbabichev.timetable.db.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ForeignKeyConstraintViolation extends RuntimeException {

    private final String                    id;
    private final Map<String, List<String>> dependents;
    private final static ObjectMapper MAPPER = new ObjectMapper();

    public ForeignKeyConstraintViolation(String id, Map<String, List<String>> dependents, String message) {
        super(message);
        this.id = id;
        this.dependents = dependents;
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        try {
            return MAPPER.writeValueAsString(new HashMap<String, Object>() {{
                put("exception", ForeignKeyConstraintViolation.class.getSimpleName());
                put("foreignKey", id);
                put("dependents", dependents);
                put("message", message);
            }});
        } catch (JsonProcessingException e) {
            // ignore
        }
        return message;
    }

    @Override
    public String toString() {
        return "ForeignKeyConstraintViolation{" + "foreignKey='" + id + '\'' + ", dependents=" + dependentsStr() + '}';
    }

    private String dependentsStr() {
        try {
            return MAPPER.writeValueAsString(dependents);
        } catch (JsonProcessingException e) {
            // ignore
        }
        return "{}";
    }
}