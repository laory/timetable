package com.dbabichev.timetable.db.dao;

import com.dbabichev.timetable.db.exception.ForeignKeyConstraintViolation;
import com.dbabichev.timetable.db.exception.IntegrityConstraintViolation;
import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.model.HoursSettings;
import com.dbabichev.timetable.model.Subject;
import com.dbabichev.timetable.model.Teacher;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.dbabichev.timetable.db.ConnectionProvider.CELLS;
import static com.dbabichev.timetable.db.ConnectionProvider.SUBJECTS;
import static com.dbabichev.timetable.db.dao.AsyncDao.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 12:03 PM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MongoSubjectDao implements SubjectDao {

    private static final Logger LOG = LoggerFactory.getLogger(MongoSubjectDao.class);

    private final MongoDatabase dbConnection;
    private final ClassDao      classDao;
    private final TeacherDao    teacherDao;

    private BiFunction<List<Clazz>, Map<String, Map<String, Object>>, Map<Clazz, HoursSettings>> composeSubjectLoad =
            (classes, pureSubjectLoad) -> pureSubjectLoad.entrySet().stream().collect(toMap(entry -> {
                String clazzId = entry.getKey();
                return classes.stream().filter(clazz -> clazzId.equals(clazz.id)).findFirst()
                              .orElseGet(() -> new Clazz(clazzId, "UNKNOWN"));
            }, entry -> {
                return new HoursSettings(entry.getValue());
            }));

    public MongoSubjectDao(MongoDatabase dbConnection) {
        this.dbConnection = dbConnection;
        this.classDao = new MongoClassDao(dbConnection);
        this.teacherDao = new MongoTeacherDao(dbConnection);
    }

    @Override
    public CompletableFuture<Subject> addNewSubject(Subject newSubject) {
        final CompletableFuture<Subject> futureResult = new CompletableFuture<>();
        if (requireNonNull(newSubject, futureResult, "New subject shouldn't be null")) {
            return futureResult;
        }
        if (requireNonNull(newSubject.title, futureResult, "New subject title shouldn't be null")) {
            return futureResult;
        }
        final MongoCollection<Document> subjects = dbConnection.getCollection(SUBJECTS);
        final ObjectId id = new ObjectId();
        final Document newSubjectDocument = new Document("_id", id);
        newSubjectDocument.append("title", newSubject.title);
        newSubjectDocument.append("subjectLoad", newSubject.getPureSubjectLoad());
        if (newSubject.teacher != null && newSubject.teacher.id != null) {
            newSubjectDocument.append("teacher", new ObjectId(newSubject.teacher.id));
        }
        subjects.insertOne(newSubjectDocument, (result, t) -> {
            if (t != null) {
                LOG.error("DB request [addNewSubject: {}] has processed unsuccessfully", newSubject, t);
            } else {
                LOG.debug("DB request [addNewSubject: {}] has processed", newSubject);
            }
            wrapResult(futureResult,
                       new Subject(id.toString(), newSubject.title, newSubject.teacher, newSubject.subjectLoad),
                       t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Subject> updateSubject(Subject updatedSubject) {
        final CompletableFuture<Subject> futureResult = new CompletableFuture<>();
        if (requireNonNull(updatedSubject, futureResult, "Subject shouldn't be null")) {
            return futureResult;
        }
        if (requireNonNull(updatedSubject.id, futureResult, "Subject id shouldn't be null")) {
            return futureResult;
        }
        if (requireNonNull(updatedSubject.title, futureResult, "Subject title shouldn't be null")) {
            return futureResult;
        }
        CompletableFuture<Subject> currentSubjectFuture = getSubject(updatedSubject.id);
        final CompletableFuture<Map<String, Integer>> busyHoursPerClassFuture =
                getBusyHoursPerClass(currentSubjectFuture);
        CompletableFuture.allOf(busyHoursPerClassFuture, currentSubjectFuture).thenAccept(v -> {
            Map<String, Integer> busyHoursPerClass = busyHoursPerClassFuture.join();
            Subject currentSubject = currentSubjectFuture.join();
            boolean newHoursPerClassAreValid =
                    validateHoursPerClass(busyHoursPerClass, updatedSubject.getPureHoursPerClass());
            boolean newDivisibilityIsValid =
                    validateDivisibilityPerClass(busyHoursPerClass, currentSubject, updatedSubject);
            if (newHoursPerClassAreValid && newDivisibilityIsValid) {
                updateSubject(futureResult, updatedSubject);
            } else {
                if (!newHoursPerClassAreValid) {
                    futureResult.completeExceptionally(new IntegrityConstraintViolation(updatedSubject.id,
                                                                                        "subjectLoad",
                                                                                        "It is not allowed to set less hours than are already in use"));
                }
                if (!newDivisibilityIsValid) {
                    futureResult.completeExceptionally(new IntegrityConstraintViolation(updatedSubject.id,
                                                                                        "isDividable",
                                                                                        "It is not allowed to disable divisibility for subjects that are already in use"));
                }
            }
        });
        return futureResult;
    }

    private void updateSubject(CompletableFuture<Subject> futureResult, Subject existingSubject) {
        final ObjectId id = new ObjectId(existingSubject.id);
        final Map<String, Map<String, Object>> newSubjectLoad = existingSubject.getPureSubjectLoad();
        final MongoCollection<Document> subjects = dbConnection.getCollection(SUBJECTS);
        final String title = existingSubject.title;
        final String teacherId = existingSubject.teacher == null ? null : existingSubject.teacher.id;
        subjects.findOneAndUpdate(eq("_id", id),
                                  combine(set("title", title),
                                          teacherId == null ? unset("teacher")
                                                            : set("teacher", new ObjectId(teacherId)),
                                          new Document("$set", new Document("subjectLoad", newSubjectLoad))),
                                  (result, t) -> {
                                      if (t != null) {
                                          LOG.error("DB request [updateSubject: {}] has processed unsuccessfully",
                                                    existingSubject,
                                                    t);
                                      } else {
                                          LOG.debug("DB request [updateSubject: {}] has processed", existingSubject);
                                      }
                                      wrapResult(futureResult,
                                                 new Subject(id.toString(),
                                                             existingSubject.title,
                                                             existingSubject.teacher,
                                                             existingSubject.subjectLoad),
                                                 t);
                                  });
    }

    @Override
    public CompletableFuture<Boolean> deleteSubject(String subjectId) {
        final CompletableFuture<Boolean> futureResult = new CompletableFuture<>();
        if (requireNonNull(subjectId, futureResult, "Teacher ID shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> subjects = dbConnection.getCollection(SUBJECTS);
        final ObjectId id = new ObjectId(subjectId);
        CompletableFuture<Map<String, List<String>>> dependenciesFuture = getDependencies(id);
        dependenciesFuture.thenAccept(dependencies -> {
            if (dependencies.isEmpty()) {
                subjects.deleteOne(eq("_id", id), (result, t) -> {
                    if (t != null) {
                        LOG.error("DB request [deleteSubject by id: {}] has processed unsuccessfully", subjectId, t);
                    } else {
                        LOG.debug("DB request [deleteSubject by id: {}] has processed", subjectId);
                    }
                    wrapResult(futureResult, result.wasAcknowledged(), t);
                });
            } else {
                futureResult.completeExceptionally(new ForeignKeyConstraintViolation(subjectId,
                                                                                     dependencies,
                                                                                     "Unable to remove subject which other entities depend on"));
            }
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<List<Subject>> getSubjects() {
        final MongoCollection<Document> subjects = dbConnection.getCollection(SUBJECTS);
        final CompletableFuture<List<Subject>> futureResult = new CompletableFuture<>();
        subjects.find().map(document2SubjectCompletableFuture()).into(new ArrayList<>(), (result, t) -> {
            if (t != null) {
                LOG.error("DB request [getSubjects] has processed unsuccessfully", t);
            } else {
                LOG.debug("DB request [getSubjects] has processed");
            }
            flatWrapResults(futureResult, result, t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Subject> getSubject(String subjectId) {
        final MongoCollection<Document> subjects = dbConnection.getCollection(SUBJECTS);
        final CompletableFuture<Subject> futureResult = new CompletableFuture<>();
        subjects.find(eq("_id", new ObjectId(subjectId))).map(document2SubjectCompletableFuture())
                .first((result, t) -> {
                    if (t != null) {
                        LOG.error("DB request [getSubjects] has processed unsuccessfully", t);
                    } else {
                        LOG.debug("DB request [getSubjects] has processed");
                    }
                    flatWrapResult(futureResult, result, t);
                });
        return futureResult;
    }

    @SafeVarargs
    private final Collection<String> collectClassIds(Collection<String>... idIterables) {
        HashSet<String> collector = new HashSet<>();
        for (Collection<String> ids : idIterables) {
            collector.addAll(ids);
        }
        return collector;
    }

    private CompletableFuture<Teacher> getTeacher(ObjectId teacherId) {
        return Optional.ofNullable(teacherId).map(id -> teacherDao.getTeacher(id.toString())).orElseGet(() -> {
            CompletableFuture<Teacher> completableFuture = new CompletableFuture<>();
            completableFuture.complete(null);
            return completableFuture;
        });
    }

    private com.mongodb.Function<Document, CompletableFuture<Subject>> document2SubjectCompletableFuture() {
        return document -> {
            final String subjectId = document.getObjectId("_id").toString();
            final String title = document.getString("title");
            final ObjectId teacherId = document.getObjectId("teacher");
            final Map<String, Map<String, Object>> pureSubjectLoad = (Map) document.get("subjectLoad");
            final CompletableFuture<Teacher> teacherFuture = getTeacher(teacherId);
            final CompletableFuture<List<Clazz>> classesFuture =
                    classDao.getClasses(collectClassIds(pureSubjectLoad.keySet()));
            final CompletableFuture<Void> allRequiredInfo = CompletableFuture.allOf(teacherFuture, classesFuture);
            return allRequiredInfo.thenApply(v -> {
                Teacher teacher = teacherFuture.join();
                List<Clazz> classes = classesFuture.join();
                Map<Clazz, HoursSettings> subjectLoad = composeSubjectLoad.apply(classes, pureSubjectLoad);
                return new Subject(subjectId, title, teacher, subjectLoad);
            });
        };
    }

    private CompletableFuture<Map<String, List<String>>> getDependencies(ObjectId id) {
        final MongoCollection<Document> cells = dbConnection.getCollection(CELLS);
        Map<String, CompletableFuture<List<String>>> dependenciesFuture =
                new HashMap<String, CompletableFuture<List<String>>>() {{
                    put("cells", getCellsForSubject(cells, id));
                }};
        return flatWrapResults(dependenciesFuture)
                .thenApply(dependencies -> dependencies.entrySet().stream().filter(entry -> !entry.getValue().isEmpty())
                                                       .collect(Collectors.toMap(Map.Entry:: getKey,
                                                                                 Map.Entry:: getValue)));
    }

    private CompletableFuture<List<String>> getCellsForSubject(MongoCollection<Document> cells, ObjectId id) {
        final CompletableFuture<List<String>> futureCount = new CompletableFuture<>();
        cells.find(eq("subjectId", id)).into(new ArrayList<>(), (result, t) -> {
            List<String> dependents =
                    result.stream().filter(Objects:: nonNull).map(document -> document.getObjectId("_id").toString())
                          .collect(Collectors.toList());
            wrapResult(futureCount, dependents, t);
        });
        return futureCount;
    }

    private CompletableFuture<Map<String, Integer>> getBusyHoursPerClass(CompletableFuture<Subject> subject) {
        return subject.thenCompose(subj -> {
            Map<String, Integer> hoursPerClass = subj.getPureHoursPerClass();
            Set<String> classIds = hoursPerClass.keySet();
            MongoCollection<Document> cells = dbConnection.getCollection(CELLS);
            Map<String, CompletableFuture<Integer>> busyHoursPerClassFuture =
                    classIds.stream().collect(toMap(identity(), classIdStr -> {
                        ObjectId classId = new ObjectId(classIdStr);
                        ObjectId subjectId = new ObjectId(subj.id);
                        LOG.info("classId {} , subjectId {}", classId, subjectId);
                        final CompletableFuture<Long> futureCount = new CompletableFuture<>();
                        Bson cellFilter = combine(eq("classId", classId), eq("subjectId", subjectId));
                        cells.count(cellFilter, (result, t) -> wrapResult(futureCount, result, t));
                        return futureCount.thenApply(Long:: intValue);
                    }));
            return flatWrapResults(busyHoursPerClassFuture);
        });
    }

    private boolean validateHoursPerClass(Map<String, Integer> busyHoursPerClass,
                                          Map<String, Integer> newHoursPerClass) {
        return busyHoursPerClass.entrySet().stream().noneMatch(entry -> {
            String clazz = entry.getKey();
            Integer busyHours = entry.getValue();
            Integer newHours = newHoursPerClass.getOrDefault(clazz, 0);
            LOG.info("Busy hours {}, new hours {}", busyHours, newHours);
            return newHours < busyHours;
        });
    }

    private boolean validateDivisibilityPerClass(Map<String, Integer> currentBusyHoursPerClass,
                                                 Subject currentSubjectLoad, Subject newSubjectLoad) {
        Map<String, HoursSettings> transformedCurrentSubjectLoad =
                currentSubjectLoad.transformSubjectLoad(clazz -> clazz.id, Function.identity());
        Map<String, HoursSettings> transformedNewSubjectLoad =
                newSubjectLoad.transformSubjectLoad(clazz -> clazz.id, Function.identity());
        LOG.info("Current busy hours per class {}. Current subject load {}. New subject Load {}",
                 currentBusyHoursPerClass,
                 currentSubjectLoad,
                 newSubjectLoad);
        return transformedCurrentSubjectLoad.entrySet().stream().filter(entry -> entry.getValue().isDividable())
                                            .filter(entry -> currentBusyHoursPerClass.containsKey(entry.getKey()) &&
                                                             currentBusyHoursPerClass.get(entry.getKey()) > 0)
                                            .allMatch(entry -> transformedNewSubjectLoad.containsKey(entry.getKey()) &&
                                                               transformedNewSubjectLoad.get(entry.getKey())
                                                                                        .isDividable());
    }
}
