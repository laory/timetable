package com.dbabichev.timetable.db.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/19/2016
 * Time: 11:41 AM
 */
interface AsyncDao {

    Logger LOG = LoggerFactory.getLogger(AsyncDao.class);

    static <T> void wrapResult(CompletableFuture<T> wrapper, T result, Throwable t) {
        if (t != null) {
            wrapper.completeExceptionally(t);
        } else {
            wrapper.complete(result);
        }
    }

    static <T> void flatWrapResults(CompletableFuture<List<T>> wrapper, List<CompletableFuture<T>> result,
                                    Throwable t) {
        if (t != null) {
            wrapper.completeExceptionally(t);
        } else {
            CompletableFuture.allOf(result.toArray(new CompletableFuture[result.size()])).thenApply(voidRes -> {
                List<T> collectedResult = result.stream().map(CompletableFuture:: join).collect(Collectors.toList());
                return wrapper.complete(collectedResult);
            });
        }
    }

    static <T> CompletableFuture<List<T>> flatWrapResults(List<CompletableFuture<T>> result) {
        CompletableFuture<List<T>> wrapper = new CompletableFuture<>();
        flatWrapResults(wrapper, result, null);
        return wrapper;
    }

    static <K, V> CompletableFuture<Map<K, V>> flatWrapResults(Map<K, CompletableFuture<V>> mapWithFutureResults) {
        CompletableFuture[] futures =
                mapWithFutureResults.values().toArray(new CompletableFuture[mapWithFutureResults.size()]);
        CompletableFuture<Void> joinedFutures = CompletableFuture.allOf(futures);
        return joinedFutures.thenApply(v -> {
            Set<Map.Entry<K, CompletableFuture<V>>> entries = mapWithFutureResults.entrySet();
            return entries.stream().collect(toMap(Map.Entry:: getKey, entry -> entry.getValue().join()));
        });
    }

    static <T> void flatWrapResult(CompletableFuture<T> wrapper, CompletableFuture<T> result, Throwable t) {
        if (t != null) {
            wrapper.completeExceptionally(t);
        } else {
            result.thenApply(wrapper:: complete);
        }
    }

    static <T> CompletableFuture<T> flatWrapResult(CompletableFuture<T> result) {
        CompletableFuture<T> wrapper = new CompletableFuture<>();
        flatWrapResult(wrapper, result, null);
        return wrapper;
    }

    static <T> boolean requireNonNull(Object value, CompletableFuture<T> futureResult, String message) {
        if (value == null) {
            futureResult.completeExceptionally(new NullPointerException(message));
            return true;
        }
        return false;
    }
}
