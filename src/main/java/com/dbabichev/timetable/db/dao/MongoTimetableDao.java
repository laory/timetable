package com.dbabichev.timetable.db.dao;

import com.dbabichev.timetable.model.Cell;
import com.mongodb.Function;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReturnDocument;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.dbabichev.timetable.db.ConnectionProvider.CELLS;
import static com.dbabichev.timetable.db.dao.AsyncDao.requireNonNull;
import static com.dbabichev.timetable.db.dao.AsyncDao.wrapResult;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 12:03 PM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MongoTimetableDao implements TimetableDao {

    private static final Logger LOG = LoggerFactory.getLogger(MongoTimetableDao.class);

    private final MongoDatabase dbConnection;
    private final SubjectDao    subjectDao;
    private Function<Document, Cell> document2cell = document -> new Cell(document.getObjectId("classId").toString(),
                                                                          document.getInteger("day"),
                                                                          document.getInteger("lesson"),
                                                                          document.getObjectId("subjectId").toString(),
                                                                          document.getInteger("division", 0));

    public MongoTimetableDao(MongoDatabase dbConnection) {
        this.dbConnection = dbConnection;
        this.subjectDao = new MongoSubjectDao(dbConnection);
    }

    @Override
    public CompletableFuture<Cell> saveTimetableCell(Cell cell) {
        final CompletableFuture<Cell> futureResult = new CompletableFuture<>();
        final MongoCollection<Document> cells = dbConnection.getCollection(CELLS);
        final Document newCellDocument = new Document();
        ObjectId classId = new ObjectId(cell.clazz);
        ObjectId subjectId = new ObjectId(cell.subject);
        newCellDocument.append("classId", classId);
        newCellDocument.append("subjectId", subjectId);
        newCellDocument.append("lesson", cell.lesson);
        newCellDocument.append("day", cell.day);
        newCellDocument.append("division", cell.division);
        cells.findOneAndReplace(and(eq("classId", classId),
                                    eq("lesson", cell.lesson),
                                    eq("day", cell.day),
                                    eq("division", cell.division)),
                                newCellDocument,
                                new FindOneAndReplaceOptions().upsert(true).returnDocument(ReturnDocument.AFTER),
                                (result, t) -> {
                                    if (t != null) {
                                        LOG.error("DB request [saveTimetableCell: {}] has processed unsuccessfully",
                                                  result,
                                                  t);
                                    } else {
                                        LOG.debug("DB request [saveTimetableCell: {}] has processed", result);
                                    }
                                    wrapResult(futureResult,
                                               new Cell(cell.clazz, cell.day, cell.lesson, cell.subject, cell.division),
                                               t);
                                });
        return futureResult;
    }

    @Override
    public CompletableFuture<Boolean> deleteTimetableCell(Cell cell) {
        final CompletableFuture<Boolean> futureResult = new CompletableFuture<>();
        if (requireNonNull(cell, futureResult, "Cell shouldn't be null")) return futureResult;
        final MongoCollection<Document> cells = dbConnection.getCollection(CELLS);
        ObjectId classId = new ObjectId(cell.clazz);
        ObjectId subjectId = new ObjectId(cell.subject);
        cells.findOneAndDelete(and(eq("classId", classId),
                                   eq("lesson", cell.lesson),
                                   eq("day", cell.day),
                                   eq("subjectId", subjectId),
                                   eq("division", cell.division)), (result, t) -> {
            if (t != null) {
                LOG.error("DB request [deleteTimetableCell by id: {}] has processed unsuccessfully", cell, t);
            } else {
                LOG.debug("DB request [deleteTimetableCell by id: {}] has processed", cell);
            }
            wrapResult(futureResult, result != null, t);
        });
        return futureResult;
    }

    public CompletableFuture<List<Cell>> getTimetableCells() {
        MongoCollection<Document> cells = dbConnection.getCollection(CELLS);
        CompletableFuture<List<Cell>> futureResult = new CompletableFuture<>();
        cells.find().map(document2cell).into(new ArrayList<>(), (result, t) -> {
            if (t != null) {
                LOG.error("DB request [getTimetableCells] has processed unsuccessfully", t);
            } else {
                LOG.debug("DB request [getTimetableCells] has processed");
            }
            wrapResult(futureResult, result, t);
        });
        return futureResult;
    }
}
