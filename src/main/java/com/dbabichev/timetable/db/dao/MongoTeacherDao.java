package com.dbabichev.timetable.db.dao;

import com.dbabichev.timetable.db.exception.ForeignKeyConstraintViolation;
import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.model.Teacher;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.dbabichev.timetable.db.ConnectionProvider.SUBJECTS;
import static com.dbabichev.timetable.db.ConnectionProvider.TEACHERS;
import static com.dbabichev.timetable.db.dao.AsyncDao.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 12:03 PM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MongoTeacherDao implements TeacherDao {

    private static final Logger LOG = LoggerFactory.getLogger(MongoTeacherDao.class);

    private final MongoDatabase dbConnection;
    private final ClassDao      classDao;

    public MongoTeacherDao(MongoDatabase dbConnection) {
        this.dbConnection = dbConnection;
        this.classDao = new MongoClassDao(dbConnection);
    }

    @Override
    public CompletableFuture<List<Teacher>> getTeachers() {
        final MongoCollection<Document> teachers = dbConnection.getCollection(TEACHERS);
        final CompletableFuture<List<Teacher>> futureResult = new CompletableFuture<>();
        teachers.find().map(this :: getTeacherCompletableFuture).into(new ArrayList<>(), (result, t) -> {
            if (t != null) {
                LOG.error("DB request [getTeachers] has processed unsuccessfully", t);
            } else {
                LOG.debug("DB request [getTeachers] has processed");
            }
            flatWrapResults(futureResult, result, t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Teacher> getTeacher(String teacherId) {
        final CompletableFuture<Teacher> futureResult = new CompletableFuture<>();
        if (requireNonNull(teacherId, futureResult, "Teacher ID shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> teachers = dbConnection.getCollection(TEACHERS);
        final ObjectId id = new ObjectId(teacherId);
        teachers.find(eq("_id", id)).map(this :: getTeacherCompletableFuture).first((result, t) -> {
            if (t != null) {
                LOG.error("DB request [getTeacher by id: {}] has processed unsuccessfully", teacherId, t);
            } else {
                LOG.debug("DB request [getTeachers by id: {}] has processed", teacherId);
            }
            flatWrapResult(futureResult, result, t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Teacher> addNewTeacher(Teacher newTeacher) {
        final CompletableFuture<Teacher> futureResult = new CompletableFuture<>();
        if (requireNonNull(newTeacher, futureResult, "New teacher shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> teachers = dbConnection.getCollection(TEACHERS);
        final ObjectId id = new ObjectId();
        final Document newTeacherDocument = new Document("name", newTeacher.name);
        newTeacherDocument.append("_id", id);
        newTeacherDocument.append("isManager", newTeacher.isManager);
        Optional.ofNullable(newTeacher.managedClass).flatMap(managedClass -> Optional.ofNullable(managedClass.id))
                .filter(idStr -> !idStr.isEmpty()).map(ObjectId::new)
                .ifPresent(managedClassId -> newTeacherDocument.append("managedClass", managedClassId));
        teachers.insertOne(newTeacherDocument, (result, t) -> {
            if (t != null) {
                LOG.error("DB request [addNewTeacher: {}] has processed unsuccessfully", newTeacher, t);
            } else {
                LOG.debug("DB request [addNewTeacher: {}] has processed", newTeacher);
            }
            wrapResult(futureResult,
                       new Teacher(id.toString(), newTeacher.name, newTeacher.isManager, newTeacher.managedClass),
                       t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Teacher> updateTeacher(Teacher existingTeacher) {
        final CompletableFuture<Teacher> futureResult = new CompletableFuture<>();
        if (requireNonNull(existingTeacher, futureResult, "Teacher shouldn't be null"))
            return futureResult;
        if (requireNonNull(existingTeacher.id, futureResult, "Teacher id shouldn't be null"))
            return futureResult;
        final String idStr = existingTeacher.id;
        final String name = existingTeacher.name;
        final boolean isManager = existingTeacher.isManager;
        final String managedClassId = existingTeacher.managedClass != null ? existingTeacher.managedClass.id : null;
        final MongoCollection<Document> teachers = dbConnection.getCollection(TEACHERS);
        final ObjectId id = new ObjectId(idStr);
        teachers.findOneAndUpdate(eq("_id", id),
                                  combine(set("name", name),
                                          set("isManager", isManager),
                                          managedClassId == null ? unset("managedClass")
                                                                 : set("managedClass", new ObjectId(managedClassId))),
                                  (result, t) -> {
                                      if (t != null) {
                                          LOG.error("DB request [updateTeacher: {}] has processed unsuccessfully",
                                                    existingTeacher,
                                                    t);
                                      } else {
                                          LOG.debug("DB request [updateTeacher: {}] has processed", existingTeacher);
                                      }
                                      wrapResult(futureResult,
                                                 new Teacher(idStr,
                                                             existingTeacher.name,
                                                             existingTeacher.isManager,
                                                             existingTeacher.managedClass),
                                                 t);
                                  });
        return futureResult;
    }

    @Override
    public CompletableFuture<Boolean> deleteTeacher(String teacherId) {
        final CompletableFuture<Boolean> futureResult = new CompletableFuture<>();
        if (requireNonNull(teacherId, futureResult, "Teacher ID shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> teachers = dbConnection.getCollection(TEACHERS);
        final ObjectId id = new ObjectId(teacherId);
        CompletableFuture<Map<String, List<String>>> dependenciesFuture = getDependencies(id);
        dependenciesFuture.thenAccept(dependencies -> {
            if (dependencies.isEmpty()) {
                teachers.deleteOne(eq("_id", id), (result, t) -> {
                    if (t != null) {
                        LOG.error("DB request [deleteTeacher by id: {}] has processed unsuccessfully", teacherId, t);
                    } else {
                        LOG.debug("DB request [deleteTeacher by id: {}] has processed", teacherId);
                    }
                    wrapResult(futureResult, result.wasAcknowledged(), t);
                });
            } else {
                futureResult.completeExceptionally(new ForeignKeyConstraintViolation(teacherId,
                                                                                     dependencies,
                                                                                     "Unable to remove teacher which other entities depend on"));
            }
        });
        return futureResult;
    }

    private CompletableFuture<Teacher> getTeacherCompletableFuture(Document document) {
        if (document == null) {
            CompletableFuture<Teacher> completableFuture = new CompletableFuture<>();
            completableFuture.complete(null);
            return completableFuture;
        } else {
            String teacherId = document.getObjectId("_id").toString();
            String name = document.getString("name");
            Boolean isManager = document.getBoolean("isManager");
            ObjectId managedClassId = document.getObjectId("managedClass");
            return getClassCompletableFuture(managedClassId)
                    .thenApply(clazz -> new Teacher(teacherId, name, isManager, clazz));
        }
    }

    private CompletableFuture<Clazz> getClassCompletableFuture(ObjectId managedClassId) {
        if (managedClassId == null) {
            CompletableFuture<Clazz> completableFuture = new CompletableFuture<>();
            completableFuture.complete(null);
            return completableFuture;
        } else {
            return classDao.getClass(managedClassId.toString());
        }
    }

    private CompletableFuture<Map<String, List<String>>> getDependencies(ObjectId id) {
        final MongoCollection<Document> subjects = dbConnection.getCollection(SUBJECTS);
        Map<String, CompletableFuture<List<String>>> dependenciesFuture =
                new HashMap<String, CompletableFuture<List<String>>>() {{
                    put("subjects", getSubjectsForTeacher(subjects, id));
                }};
        return flatWrapResults(dependenciesFuture)
                .thenApply(dependencies -> dependencies.entrySet().stream().filter(entry -> !entry.getValue().isEmpty())
                                                       .collect(Collectors.toMap(Map.Entry:: getKey,
                                                                                 Map.Entry:: getValue)));
    }

    private CompletableFuture<List<String>> getSubjectsForTeacher(MongoCollection<Document> subjects, ObjectId id) {
        final CompletableFuture<List<String>> futureDependentTeachers = new CompletableFuture<>();
        subjects.find(eq("teacher", id)).into(new ArrayList<>(), (result, t) -> {
            List<String> dependencies =
                    result.stream().filter(Objects:: nonNull).map(document -> document.getObjectId("_id").toString())
                          .collect(Collectors.toList());
            AsyncDao.wrapResult(futureDependentTeachers, dependencies, t);
        });
        return futureDependentTeachers;
    }
}
