package com.dbabichev.timetable.db.dao;

import com.dbabichev.timetable.model.Subject;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/19/2016
 * Time: 11:40 AM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface SubjectDao extends AsyncDao {

    CompletableFuture<List<Subject>> getSubjects();

    CompletableFuture<Subject> getSubject(String subjectId);

    CompletableFuture<Subject> addNewSubject(Subject newSubject);

    CompletableFuture<Boolean> deleteSubject(String subjectId);

    CompletableFuture<Subject> updateSubject(Subject existingSubject);
}
