package com.dbabichev.timetable.db.dao;

import com.dbabichev.timetable.db.exception.ForeignKeyConstraintViolation;
import com.dbabichev.timetable.model.Clazz;
import com.mongodb.Function;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.dbabichev.timetable.db.ConnectionProvider.*;
import static com.dbabichev.timetable.db.dao.AsyncDao.requireNonNull;
import static com.dbabichev.timetable.db.dao.AsyncDao.wrapResult;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Updates.set;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 12:03 PM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MongoClassDao implements ClassDao {

    private static final Logger LOG = LoggerFactory.getLogger(MongoClassDao.class);

    private final MongoDatabase dbConnection;
    private Function<Document, Clazz> document2class =
            document -> new Clazz(document.getObjectId("_id").toString(), document.getString("title"));

    public MongoClassDao(MongoDatabase dbConnection) {
        this.dbConnection = dbConnection;
    }

    @Override
    public CompletableFuture<List<Clazz>> getClasses() {
        MongoCollection<Document> classes = dbConnection.getCollection(CLASSES);
        CompletableFuture<List<Clazz>> futureResult = new CompletableFuture<>();
        classes.find().map(document2class).into(new ArrayList<>(), (result, t) -> {
            if (t != null) {
                LOG.error("DB request [getClasses] has processed unsuccessfully", t);
            } else {
                LOG.debug("DB request [getClasses] has processed");
            }
            wrapResult(futureResult, result, t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<List<Clazz>> getClasses(Collection<String> ids) {
        MongoCollection<Document> classes = dbConnection.getCollection(CLASSES);
        CompletableFuture<List<Clazz>> futureResult = new CompletableFuture<>();
        classes.find(in("_id", idObjects(ids))).map(document2class).into(new ArrayList<>(), (result, t) -> {
            if (t != null) {
                LOG.error("DB request [getClasses] by ids: {} has processed unsuccessfully", ids, t);
            } else {
                LOG.debug("DB request [getClasses] by ids: {} has processed", ids);
            }
            wrapResult(futureResult, result, t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Clazz> getClass(String classId) {
        final CompletableFuture<Clazz> futureResult = new CompletableFuture<>();
        if (requireNonNull(classId, futureResult, "Class ID shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> classes = dbConnection.getCollection(CLASSES);
        final ObjectId id = new ObjectId(classId);
        classes.find(eq("_id", id)).map(document -> {
            if (document == null) {
                return null;
            }
            return new Clazz(classId, document.getString("title"));
        }).first((result, t) -> {
            if (t != null) {
                LOG.error("DB request [getClass by id: {}] has processed unsuccessfully", classId, t);
            } else {
                LOG.debug("DB request [getClass by id: {}] has processed", classId);
            }
            wrapResult(futureResult, result, t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Clazz> addNewClass(Clazz newClass) {
        final CompletableFuture<Clazz> futureResult = new CompletableFuture<>();
        if (requireNonNull(newClass, futureResult, "New class shouldn't be null"))
            return futureResult;
        if (requireNonNull(newClass.title, futureResult, "New class title shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> classes = dbConnection.getCollection(CLASSES);
        final ObjectId id = new ObjectId();
        final Document newClassDocument = new Document("_id", id);
        newClassDocument.append("title", newClass.title);
        classes.insertOne(newClassDocument, (result, t) -> {
            if (t != null) {
                LOG.error("DB request [addNewClass: {}] has processed unsuccessfully", newClass, t);
            } else {
                LOG.debug("DB request [addNewClass: {}] has processed", newClass);
            }
            wrapResult(futureResult, new Clazz(id.toString(), newClass.title), t);
        });
        return futureResult;
    }

    @Override
    public CompletableFuture<Boolean> deleteClass(String classId) {
        final CompletableFuture<Boolean> futureResult = new CompletableFuture<>();
        if (requireNonNull(classId, futureResult, "Class ID shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> classes = dbConnection.getCollection(CLASSES);
        final ObjectId id = new ObjectId(classId);

        CompletableFuture<Map<String, List<String>>> dependenciesFuture = findDependencies(id);

        dependenciesFuture.thenAccept(dependencies -> {
            if (dependencies.isEmpty()) {
                classes.deleteOne(eq("_id", id), (result, t) -> {
                    if (t != null) {
                        LOG.error("DB request [deleteClass by id: {}] has processed unsuccessfully", classId, t);
                    } else {
                        LOG.debug("DB request [deleteClass by id: {}] has processed", classId);
                    }
                    wrapResult(futureResult, result.wasAcknowledged(), t);
                });
            } else {
                futureResult.completeExceptionally(new ForeignKeyConstraintViolation(classId,
                                                                                     dependencies,
                                                                                     "Unable to remove class which other entities depend on"));
            }
        });

        return futureResult;
    }

    @Override
    public CompletableFuture<Clazz> updateClass(Clazz existingClass) {
        final CompletableFuture<Clazz> futureResult = new CompletableFuture<>();
        if (requireNonNull(existingClass, futureResult, "Class shouldn't be null"))
            return futureResult;
        if (requireNonNull(existingClass.id, futureResult, "Class id shouldn't be null"))
            return futureResult;
        if (requireNonNull(existingClass.title, futureResult, "Class title shouldn't be null"))
            return futureResult;
        final String idStr = existingClass.id;
        final String title = existingClass.title;
        if (requireNonNull(idStr, futureResult, "Class id shouldn't be null"))
            return futureResult;
        final MongoCollection<Document> classes = dbConnection.getCollection(CLASSES);
        final ObjectId id = new ObjectId(idStr);
        classes.findOneAndUpdate(eq("_id", id), set("title", title), (result, t) -> {
            if (t != null) {
                LOG.error("DB request [updateClass: {}] has processed unsuccessfully", existingClass, t);
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("DB request [updateClass: {}] has processed", existingClass);
                }
            }
            wrapResult(futureResult, new Clazz(idStr, title), t);
        });
        return futureResult;
    }

    private CompletableFuture<Map<String, List<String>>> findDependencies(ObjectId id) {
        final MongoCollection<Document> teachers = dbConnection.getCollection(TEACHERS);
        final MongoCollection<Document> subjects = dbConnection.getCollection(SUBJECTS);
        final MongoCollection<Document> cells = dbConnection.getCollection(CELLS);

        HashMap<String, CompletableFuture<List<String>>> dependenciesFuture =
                new HashMap<String, CompletableFuture<List<String>>>() {{
                    put("teachers", getDependentClassManagers(teachers, id));
                    put("subjects", getDependentSubjectsForClass(subjects, id));
                    put("cells", getDependentCellsForClass(cells, id));
                }};

        return AsyncDao.flatWrapResults(dependenciesFuture).thenApply(dependencies -> dependencies.entrySet().stream()
                                                                                                  .filter(entry -> !entry
                                                                                                          .getValue()
                                                                                                          .isEmpty())
                                                                                                  .collect(Collectors
                                                                                                                   .toMap(Map.Entry:: getKey,
                                                                                                                          Map.Entry:: getValue)));
    }

    private CompletableFuture<List<String>> getDependentClassManagers(MongoCollection<Document> teachers, ObjectId id) {
        final CompletableFuture<List<String>> futureCount = new CompletableFuture<>();
        teachers.find(eq("managedClass", id)).into(new ArrayList<>(), (result, t) -> {
            List<String> classManagers =
                    result.stream().filter(Objects:: nonNull).map(document -> document.getObjectId("_id").toString())
                          .collect(Collectors.toList());
            AsyncDao.wrapResult(futureCount, classManagers, t);
        });
        return futureCount;
    }

    private CompletableFuture<List<String>> getDependentSubjectsForClass(MongoCollection<Document> subjects,
                                                                         ObjectId id) {
        final CompletableFuture<List<String>> futureCount = new CompletableFuture<>();
        subjects.find().into(new LinkedList<>(), (result, t) -> {
            if (t != null) {
                AsyncDao.wrapResult(futureCount, Collections.emptyList(), t);
            } else {
                String idStr = id.toString();
                List<String> dependentSubjects = result.stream().filter(document -> {
                    Map<String, Integer> subjectLoad = (Map) document.get("subjectLoad");
                    return subjectLoad != null &&
                           subjectLoad.keySet().stream().anyMatch(clazz -> Objects.equals(clazz, idStr));
                }).map(document -> document.getObjectId("_id").toString()).collect(Collectors.toList());
                AsyncDao.wrapResult(futureCount, dependentSubjects, null);
            }
        });
        return futureCount;
    }

    private CompletableFuture<List<String>> getDependentCellsForClass(MongoCollection<Document> cells, ObjectId id) {
        final CompletableFuture<List<String>> futureCount = new CompletableFuture<>();
        cells.find(eq("classId", id)).into(new ArrayList<>(), (result, t) -> {
            List<String> dependentCells = result.stream().map(document -> document.getObjectId("_id").toString())
                                                .collect(Collectors.toList());
            AsyncDao.wrapResult(futureCount, dependentCells, t);
        });
        return futureCount;
    }

    private List<ObjectId> idObjects(Collection<String> ids) {
        return ids.stream().map(ObjectId::new).collect(Collectors.toList());
    }
}
