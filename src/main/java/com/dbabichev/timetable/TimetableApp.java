package com.dbabichev.timetable;

import com.dbabichev.timetable.controller.*;
import com.dbabichev.timetable.web_socket.TimetableWebSocket;

import static spark.Spark.*;

/**
 * Created with IntelliJ IDEA.
 * Date: 24/8/16
 * Time: 1:10 AM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TimetableApp {

    public static void main(String[] args) {
        staticFileLocation("/public");

        webSocket("/timetable", TimetableWebSocket.class);

        put("/classes/class", ClassController.addNewClass, Controller::toJson);
        put("/classes/class/update", ClassController.updateClass, Controller:: toJson);
        delete("/classes/class/:classId", ClassController.deleteClass, Controller:: toJson);
        get("/classes/class/:classId", ClassController.getClass, Controller:: toJson);
        get("/classes", ClassController.getClasses, Controller:: toJson);

        put("/teachers/teacher", TeacherController.addNewTeacher, Controller:: toJson);
        put("/teachers/teacher/update", TeacherController.updateTeacher, Controller:: toJson);
        delete("/teachers/teacher/:teacherId", TeacherController.deleteTeacher, Controller:: toJson);
        get("/teachers/teacher/:teacherId", TeacherController.getTeacher, Controller:: toJson);
        get("/teachers", TeacherController.getTeachers, Controller:: toJson);

        put("/subjects/subject", SubjectController.addNewSubject, Controller:: toJson);
        put("/subjects/subject/update", SubjectController.updateSubject, Controller:: toJson);
        delete("/subjects/subject/:subjectId", SubjectController.deleteSubject, Controller:: toJson);
        get("/subjects/subject/:subjectId", SubjectController.getSubject, Controller:: toJson);
        get("/subjects", SubjectController.getSubjects, Controller:: toJson);

        get("/timetableCells", TimetableController.getCells, Controller:: toJson);

        get("/export/timetable", TimetableExportController.exportTimetableToExcel);
        get("/export/teacher_timetable", TeacherTimetableExportController.exportTimetableToExcel);

        exception(Exception.class, ExceptionController.handleException);
    }
}
