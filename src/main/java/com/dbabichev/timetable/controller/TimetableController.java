package com.dbabichev.timetable.controller;

import com.dbabichev.timetable.db.ConnectionProvider;
import com.dbabichev.timetable.db.dao.MongoTimetableDao;
import com.dbabichev.timetable.db.dao.TimetableDao;
import spark.Route;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 31/12/16
 * Time: 1:50 AM
 */
public class TimetableController extends Controller {

    private static final TimetableDao timetableDao = new MongoTimetableDao(ConnectionProvider.getConnection());

    public static Route getCells = (req, res) -> timetableDao.getTimetableCells().get();
}
