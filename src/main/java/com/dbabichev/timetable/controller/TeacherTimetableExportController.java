package com.dbabichev.timetable.controller;

import com.dbabichev.timetable.db.ConnectionProvider;
import com.dbabichev.timetable.db.dao.*;
import com.dbabichev.timetable.model.Cell;
import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.model.Subject;
import com.dbabichev.timetable.model.Teacher;
import com.mongodb.async.client.MongoDatabase;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import spark.Route;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;
import static java.util.stream.IntStream.rangeClosed;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 1/25/2017
 * Time: 10:44 AM
 */
public class TeacherTimetableExportController extends ExportController {

    public static final  String       EXCEL_TIMETABLE_TEMPLATE = "/templates/excelTeacherTimetable.template.xlsx";
    private static final Point        DAYS_OFFSET              = new Point(1, 1);
    private static final Point        LESSONS_OFFSET           = new Point(1, 0);
    private static final Point        CLASSES_OFFSET           = new Point(1, 1);
    private static final Point        TEACHERS_OFFSET          = new Point(0, 1);
    public static final  int          LESSONS                  = 7;
    public static final  List<String> DAYS                     =
            Arrays.asList("ПОНЕДІЛОК", "ВІВТОРОК", "СЕРЕДА", "ЧЕТВЕР", "П'ЯТНИЦЯ");

    public static Route exportTimetableToExcel = (request, response) -> {
        MongoDatabase connection = ConnectionProvider.getConnection();
        TimetableDao timetableDao = new MongoTimetableDao(connection);
        ClassDao classDao = new MongoClassDao(connection);
        TeacherDao teacherDao = new MongoTeacherDao(connection);
        SubjectDao subjectDao = new MongoSubjectDao(connection);
        CompletableFuture<List<Cell>> timetableCellsFuture = timetableDao.getTimetableCells();
        CompletableFuture<List<Clazz>> classesFuture = classDao.getClasses();
        CompletableFuture<List<Teacher>> teachersFuture = teacherDao.getTeachers();
        CompletableFuture<List<Subject>> subjectsFuture = subjectDao.getSubjects();
        CompletableFuture.allOf(timetableCellsFuture, classesFuture, teachersFuture, subjectsFuture).thenAccept(v -> {
            List<Cell> cells = timetableCellsFuture.join();
            List<Clazz> classes = classesFuture.join();
            List<Teacher> teachers = teachersFuture.join();
            List<Subject> subjects = subjectsFuture.join();
            teachers.sort((teacher1, teacher2) -> teacher1.name.compareToIgnoreCase(teacher2.name));
            Workbook timetable = generateTimetable(cells, classes, teachers, subjects);
            writeResponse(response, timetable, "teacherTimetable.xlsx");
        }).get();
        return response.raw();
    };

    private static Workbook generateTimetable(List<Cell> cells, List<Clazz> classes, List<Teacher> teachers,
                                              List<Subject> subjects) {
        try {
            Map<String, Subject> subjectsCache =
                    subjects.stream().collect(Collectors.groupingBy(subject -> subject.id)).entrySet().stream()
                            .collect(Collectors.toMap(Map.Entry:: getKey, entry -> entry.getValue().get(0)));
            Map<String, Clazz> classesCache =
                    classes.stream().collect(Collectors.groupingBy(clazz -> clazz.id)).entrySet().stream()
                           .collect(Collectors.toMap(Map.Entry:: getKey, entry -> entry.getValue().get(0)));
            Workbook workbook = new XSSFWorkbook(TeacherTimetableExportController.class
                                                         .getResourceAsStream(EXCEL_TIMETABLE_TEMPLATE));
            Sheet timetableSheet = workbook.getSheetAt(0);
            range(0, teachers.size()).forEach(teacherIndex -> {
                Teacher teacher = teachers.get(teacherIndex);
                setTeacher(timetableSheet, teacherIndex, teacher.name);
                range(0, DAYS.size()).forEach(day -> {
                    rangeClosed(0, LESSONS).forEach(lesson -> {
                        Optional<Cell> timetableCell = getTimetableCell(cells, teacher.id, day, lesson, subjectsCache);
                        String classId = timetableCell.map(cell -> cell.clazz).orElse("notFound");
                        Clazz clazz = classesCache.getOrDefault(classId, new Clazz(null, ""));
                        setClass(timetableSheet, day, lesson, teacherIndex, clazz.title);
                        setLesson(timetableSheet, day, lesson);
                    });
                });
            });
            range(0, DAYS.size()).forEach(day -> setDay(workbook, timetableSheet, day, teachers.size()));
            return workbook;
        } catch (IOException e) {
            throw new ExportController.ExportException("Unable to generate teacher timetable", e);
        }
    }

    private static Optional<Cell> getTimetableCell(List<Cell> cells, String teacherId, int day, int lesson,
                                                   Map<String, Subject> subjectsCache) {
        return cells.stream().filter(cell -> {
            Teacher defaultTeacher = new Teacher(null, "NOT_FOUND", false, null);
            Subject defaultSubject = new Subject(null, defaultTeacher, null);
            Subject foundSubject = subjectsCache.getOrDefault(cell.subject, defaultSubject);
            Subject subject = foundSubject.teacher == null ? defaultSubject : foundSubject;
            return Objects.equals(subject.teacher.id, teacherId) && cell.day == day && cell.lesson == lesson;
        }).findFirst();
    }

    private static void setDay(Workbook workbook, Sheet timetableSheet, int day, int columnsCount) {
        Row row = getRow(timetableSheet, getFirstDayRow(day));
        rangeClosed(0, columnsCount).forEach(columnIndex -> {
            org.apache.poi.ss.usermodel.Cell cell = getCell(row, columnIndex);
            CellStyle cellStyle = cell.getCellStyle();
            CellStyle templateCellStyle = workbook.createCellStyle();
            templateCellStyle.cloneStyleFrom(cellStyle);
            templateCellStyle.setBorderTop(BorderStyle.MEDIUM);
            cell.setCellStyle(templateCellStyle);
        });
    }

    private static void setLesson(Sheet timetableSheet, int day, int lesson) {
        Row row = getRow(timetableSheet, getLessonRow(day, lesson));
        org.apache.poi.ss.usermodel.Cell cell = getCell(row, LESSONS_OFFSET.column);
        CellStyle cellStyle = getCell(getRow(timetableSheet, LESSONS_OFFSET.row), LESSONS_OFFSET.column).getCellStyle();
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.MEDIUM);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("" + lesson + ".");
    }

    private static void setTeacher(Sheet timetableSheet, int teacherIndex, String name) {
        Row row = getRow(timetableSheet, TEACHERS_OFFSET.row);
        org.apache.poi.ss.usermodel.Cell cell = getCell(row, TEACHERS_OFFSET.column + teacherIndex);
        CellStyle cellStyle = getCell(row, TEACHERS_OFFSET.column).getCellStyle();
        cell.setCellStyle(cellStyle);
        cell.setCellValue(name);
    }

    private static void setClass(Sheet timetableSheet, int day, int lesson, int teacherIndex, String title) {
        Row row = getRow(timetableSheet, getLessonRow(day, lesson));
        org.apache.poi.ss.usermodel.Cell cell = getCell(row, teacherIndex + CLASSES_OFFSET.column);
        CellStyle cellStyle = getCell(getRow(timetableSheet, CLASSES_OFFSET.row), CLASSES_OFFSET.column).getCellStyle();
        cellStyle.cloneStyleFrom(cellStyle);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(title);
    }

    private static int getDayOffset(int day) {
        return day * (LESSONS + 1);
    }

    private static int getFirstDayRow(int day) {
        return getDayOffset(day) + DAYS_OFFSET.row;
    }

    private static int getLessonRow(int day, int lesson) {
        return getFirstLessonRow(day) + lesson;
    }

    private static int getFirstLessonRow(int day) {
        return getDayOffset(day) + LESSONS_OFFSET.row;
    }

}
