package com.dbabichev.timetable.controller;

import com.dbabichev.timetable.db.ConnectionProvider;
import com.dbabichev.timetable.db.dao.MongoSubjectDao;
import com.dbabichev.timetable.db.dao.SubjectDao;
import com.dbabichev.timetable.model.Subject;
import com.dbabichev.timetable.service.SubjectService;
import com.fasterxml.jackson.databind.JsonNode;
import spark.Route;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 11:30 AM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class SubjectController extends Controller {

    private static final SubjectDao     subjectDao           = new MongoSubjectDao(ConnectionProvider.getConnection());
    private static final SubjectService subjectService       = new SubjectService(subjectDao);

    private static final String         SUBJECT_MODIFICATION = "subjectModification";
    private static final String         UPDATE_SUBJECT       = "updateSubject";
    private static final String         DELETE_SUBJECT       = "deleteSubject";

    public static Route addNewSubject = (req, res) -> {
        JsonNode requestNode = mapRequest(req);
        final String title = requestNode.hasNonNull("title") ? requestNode.get("title").asText() : null;
        final String teacherId = requestNode.hasNonNull("teacher") ? requestNode.get("teacher").asText() : null;
        final Map<String, Map<String, Object>> subjectLoad = MAPPER.convertValue(requestNode.get("subjectLoad"), Map.class);
        Subject addedSubject = subjectService.addNewSubject(title, teacherId, subjectLoad).get();
        broadcast(SUBJECT_MODIFICATION, UPDATE_SUBJECT, "subject", addedSubject);
        return addedSubject;
    };

    public static Route updateSubject = (req, res) -> {
        JsonNode requestNode = mapRequest(req);
        final String id = requestNode.hasNonNull("id") ? requestNode.get("id").asText() : null;
        final String title = requestNode.hasNonNull("title") ? requestNode.get("title").asText() : null;
        final String teacherId = requestNode.hasNonNull("teacher") ? requestNode.get("teacher").asText() : null;
        final Map<String, Map<String, Object>> subjectLoad = MAPPER.convertValue(requestNode.get("subjectLoad"), Map.class);
        Subject updatedSubject = subjectService.updateSubject(id, title, teacherId, subjectLoad).get();
        broadcast(SUBJECT_MODIFICATION, UPDATE_SUBJECT, "subject", updatedSubject);
        return updatedSubject;
    };

    public static Route getSubject = (req, res) -> {
        final String subjectId = req.params(":subjectId");
        return subjectService.getSubject(subjectId).get();
    };

    public static Route getSubjects = (req, res) -> subjectService.getSubjects().get();

    public static Route deleteSubject = (req, res) -> {
        final String subjectId = req.params(":subjectId");
        Boolean isRemoved = subjectService.deleteSubject(subjectId).get();
        if (isRemoved) {
            broadcast(SUBJECT_MODIFICATION, DELETE_SUBJECT, "subject", new Subject(subjectId, null, null, null));
        }
        return isRemoved;
    };
}
