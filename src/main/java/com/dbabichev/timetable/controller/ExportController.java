package com.dbabichev.timetable.controller;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import spark.Response;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 1/26/2017
 * Time: 12:52 PM
 */
public class ExportController {

    protected static void writeResponse(Response response, Workbook timetable, String fileName) {
        try {
            response.type("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            HttpServletResponse servletResponse = response.raw();
            servletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            servletResponse.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            ServletOutputStream outputStream = servletResponse.getOutputStream();
            timetable.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            throw new ExportController.ExportException("Unable to write a timetable into response", e);
        }
    }

    protected static Row getRow(Sheet timetableSheet, int row) {
        Row excelRow = timetableSheet.getRow(row);
        if (excelRow == null) {
            excelRow = timetableSheet.createRow(row);
        }
        return excelRow;
    }

    protected static org.apache.poi.ss.usermodel.Cell getCell(Row row, int cell) {
        org.apache.poi.ss.usermodel.Cell excelCell = row.getCell(cell);
        if (excelCell == null) {
            excelCell = row.createCell(cell);
        }
        return excelCell;
    }

    protected static class Point {

        public final int row;
        public final int column;

        Point(int row, int column) {
            this.row = row;
            this.column = column;
        }

        CellStyle getStyle(Sheet sheet) {
            return sheet.getRow(row).getCell(column).getCellStyle();
        }
    }

    protected static class ExportException extends RuntimeException {

        ExportException(String message, Exception cause) {
            super(message, cause);
        }
    }
}
