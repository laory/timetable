package com.dbabichev.timetable.controller;

import com.dbabichev.timetable.web_socket.handler.Handler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 25/12/16
 * Time: 12:00 AM
 */
public class Controller {

    static final ObjectMapper MAPPER = new ObjectMapper();

    static JsonNode mapRequest(Request req) {
        try {
            return MAPPER.readTree(req.body());
        } catch (IOException e) {
            throw new RequestMappingException("Unable to map request", e);
        }
    }

    public static String toJson(Object response) {
        try {
            return MAPPER.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new ResponseSerializationException("Unable to serialize response to JSON", e);
        }
    }

    public static class RequestMappingException extends RuntimeException {

        RequestMappingException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class ResponseSerializationException extends RuntimeException {

        ResponseSerializationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    static void broadcast(String routingKey, String operation, String key, Object value) throws JsonProcessingException {
        Map<String, Object> response = Collections.singletonMap(key, MAPPER.writeValueAsString(value));
        Handler.broadcast(routingKey, operation, response);
    }
}
