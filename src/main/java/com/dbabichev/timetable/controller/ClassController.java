package com.dbabichev.timetable.controller;

import com.dbabichev.timetable.db.ConnectionProvider;
import com.dbabichev.timetable.db.dao.ClassDao;
import com.dbabichev.timetable.db.dao.MongoClassDao;
import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.service.ClassService;
import com.fasterxml.jackson.databind.JsonNode;
import spark.Route;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 11:30 AM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class ClassController extends Controller {

    private static final ClassDao     classDao     = new MongoClassDao(ConnectionProvider.getConnection());
    private static final ClassService classService = new ClassService(classDao);

    private static final String CLASS_MODIFICATION = "classModification";
    private static final String UPDATE_CLASS       = "updateClass";
    private static final String DELETE_CLASS       = "deleteClass";

    public static Route addNewClass = (req, res) -> {
        JsonNode requestNode = mapRequest(req);
        final String title = requestNode.hasNonNull("title") ? requestNode.get("title").asText() : null;
        Clazz addedClass = classService.addNewClass(title).get();
        broadcast(CLASS_MODIFICATION, UPDATE_CLASS, "clazz", addedClass);
        return addedClass;
    };

    public static Route updateClass = (req, res) -> {
        JsonNode requestNode = mapRequest(req);
        String id = requestNode.hasNonNull("id") ? requestNode.get("id").asText() : null;
        String title = requestNode.hasNonNull("title") ? requestNode.get("title").asText() : null;
        Clazz updatedClass = classService.updateClass(id, title).get();
        broadcast(CLASS_MODIFICATION, UPDATE_CLASS, "clazz", updatedClass);
        return updatedClass;
    };

    public static Route getClass = (req, res) -> {
        final String classId = req.params(":classId");
        return classService.getClass(classId).get();
    };

    public static Route getClasses = (req, res) -> classService.getClasses().get();

    public static Route deleteClass = (req, res) -> {
        final String classId = req.params(":classId");
        Boolean isDeleted = classService.deleteClass(classId).get();
        if (isDeleted) {
            broadcast(CLASS_MODIFICATION, DELETE_CLASS, "clazz", new Clazz(classId, null));
        }
        return isDeleted;
    };
}
