package com.dbabichev.timetable.controller;

import com.dbabichev.timetable.db.ConnectionProvider;
import com.dbabichev.timetable.db.dao.MongoTeacherDao;
import com.dbabichev.timetable.model.Teacher;
import com.dbabichev.timetable.service.TeacherService;
import com.fasterxml.jackson.databind.JsonNode;
import spark.Route;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 11:30 AM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TeacherController extends Controller {

    private static final MongoTeacherDao teacherDao     = new MongoTeacherDao(ConnectionProvider.getConnection());
    private static final TeacherService  teacherService = new TeacherService(teacherDao);

    private static final String TEACHER_MODIFICATION = "teacherModification";
    private static final String UPDATE_TEACHER       = "updateTeacher";

    public static Route addNewTeacher = (req, res) -> {
        JsonNode requestNode = mapRequest(req);
        final String name = requestNode.hasNonNull("name") ? requestNode.get("name").asText() : null;
        final boolean isManager = requestNode.hasNonNull("isManager") && requestNode.get("isManager").asBoolean();
        final String managedClassId =
                requestNode.hasNonNull("managedClass") ? requestNode.get("managedClass").asText() : null;
        Teacher addedTeacher = teacherService.addNewTeacher(name, isManager, managedClassId).get();
        broadcast(TEACHER_MODIFICATION, UPDATE_TEACHER, "teacher", addedTeacher);
        return addedTeacher;
    };

    public static Route updateTeacher = (req, res) -> {
        JsonNode requestNode = mapRequest(req);
        final String id = requestNode.hasNonNull("id") ? requestNode.get("id").asText() : null;
        final String name = requestNode.hasNonNull("name") ? requestNode.get("name").asText() : null;
        final boolean isManager = requestNode.hasNonNull("isManager") && requestNode.get("isManager").asBoolean();
        final String managedClassId =
                requestNode.hasNonNull("managedClass") ? requestNode.get("managedClass").asText() : null;
        Teacher updatedTeacher = teacherService.updateTeacher(id, name, isManager, managedClassId).get();
        broadcast(TEACHER_MODIFICATION, UPDATE_TEACHER, "teacher", updatedTeacher);
        return updatedTeacher;
    };

    public static Route getTeacher = (req, res) -> {
        final String teacherId = req.params(":teacherId");
        return teacherService.getTeacher(teacherId).get();
    };

    public static Route getTeachers = (req, res) -> teacherService.getTeachers().get();

    public static Route deleteTeacher = (req, res) -> {
        final String teacherId = req.params(":teacherId");
        Boolean isDeleted = teacherService.deleteTeacher(teacherId).get();
        if (isDeleted) {
            broadcast(TEACHER_MODIFICATION, UPDATE_TEACHER, "teacher", new Teacher(teacherId, null, false, null));
        }
        return isDeleted;
    };
}
