package com.dbabichev.timetable.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ExceptionHandler;
import spark.Response;

import java.util.concurrent.ExecutionException;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 12/27/2016
 * Time: 11:40 AM
 */
public class ExceptionController extends Controller {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionController.class);

    public static ExceptionHandler handleException = (exception, request, response) -> {
        String requestStringified;
        try {
            JsonNode requestNode = mapRequest(request);
            requestStringified = requestNode.toString();
        } catch (Exception e) {
            requestStringified = "{}";
        }
        LOG.error("{} request {} with params {} has not been processed",
                  request.requestMethod(),
                  request.uri(),
                  requestStringified,
                  exception);
        respondWithError(response,
                         (exception instanceof ExecutionException) ? exception.getCause().getMessage() :
                         "Unable to handle your request due to internal exception: " + exception.getMessage());
    };

    private static void respondWithError(Response response, String clientMessage) {
        response.body(clientMessage);
        response.status(500);
    }
}
