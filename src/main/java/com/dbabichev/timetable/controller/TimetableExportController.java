package com.dbabichev.timetable.controller;

import com.dbabichev.timetable.db.ConnectionProvider;
import com.dbabichev.timetable.db.dao.ClassDao;
import com.dbabichev.timetable.db.dao.MongoClassDao;
import com.dbabichev.timetable.db.dao.MongoSubjectDao;
import com.dbabichev.timetable.db.dao.MongoTimetableDao;
import com.dbabichev.timetable.db.dao.SubjectDao;
import com.dbabichev.timetable.db.dao.TimetableDao;
import com.dbabichev.timetable.model.Cell;
import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.model.Subject;
import com.mongodb.async.client.MongoDatabase;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import spark.Route;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;
import static java.util.stream.IntStream.rangeClosed;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 1/25/2017
 * Time: 10:44 AM
 */
public class TimetableExportController extends ExportController {

    public static final  String       EXCEL_TIMETABLE_TEMPLATE = "/templates/excelTimetable.template.xlsx";
    private static final Point        DAYS_OFFSET              = new Point(1, 0);
    private static final Point        CLASSES_OFFSET           = new Point(0, 2);
    private static final Point        LESSONS_OFFSET           = new Point(1, 1);
    private static final Point        SUBJECTS_OFFSET          = new Point(1, 2);
    public static final  int          LESSONS                  = 7;
    public static final  List<String> DAYS                     =
            Arrays.asList("ПОНЕДІЛОК", "ВІВТОРОК", "СЕРЕДА", "ЧЕТВЕР", "П'ЯТНИЦЯ");

    public static Route exportTimetableToExcel = (request, response) -> {
        MongoDatabase connection = ConnectionProvider.getConnection();
        TimetableDao timetableDao = new MongoTimetableDao(connection);
        ClassDao classDao = new MongoClassDao(connection);
        SubjectDao subjectDao = new MongoSubjectDao(connection);
        CompletableFuture<List<Cell>> timetableCellsFuture = timetableDao.getTimetableCells();
        CompletableFuture<List<Subject>> subjectsFuture = subjectDao.getSubjects();
        CompletableFuture<List<Clazz>> classesFuture = classDao.getClasses();
        CompletableFuture.allOf(timetableCellsFuture, subjectsFuture, classesFuture).thenAccept(v -> {
            List<Cell> cells = timetableCellsFuture.join();
            List<Subject> subjects = subjectsFuture.join();
            List<Clazz> classes = classesFuture.join();
            classes.sort((class1, class2) -> class1.title.compareToIgnoreCase(class2.title));
            Workbook timetable = generateTimetable(cells, subjects, classes);
            writeResponse(response, timetable, "timetable.xlsx");
        }).get();
        return response.raw();
    };

    private static Workbook generateTimetable(List<Cell> cells, List<Subject> subjects, List<Clazz> classes) {
        try {
            Map<String, Subject> subjectsCache =
                    subjects.stream().collect(Collectors.groupingBy(subject -> subject.id)).entrySet().stream()
                            .collect(Collectors.toMap(Map.Entry:: getKey, entry -> entry.getValue().get(0)));
            Workbook workbook =
                    new XSSFWorkbook(TimetableExportController.class.getResourceAsStream(EXCEL_TIMETABLE_TEMPLATE));
            Sheet timetableSheet = workbook.getSheetAt(0);
            range(0, DAYS.size()).forEach(day -> setDay(timetableSheet, day));
            range(0, classes.size()).forEach(classIndex -> {
                Clazz clazz = classes.get(classIndex);
                setClass(timetableSheet, classIndex, clazz.title);
                range(0, DAYS.size()).forEach(day -> {
                    rangeClosed(0, LESSONS).forEach(lesson -> {
                        setLesson(timetableSheet, day, lesson);
                        String subjectTitle = getSubjectTitle(day, lesson, clazz.id, subjectsCache, cells);
                        setSubject(timetableSheet, day, classIndex, lesson, subjectTitle);
                    });
                });
            });
            return workbook;
        } catch (Exception e) {
            throw new ExportController.ExportException("Unable to generate timetable", e);
        }
    }

    private static void setSubject(Sheet timetableSheet, int day, int classIndex, int lesson, String subjectTitle) {
        Row row = getRow(timetableSheet, getLessonRow(day, lesson));
        org.apache.poi.ss.usermodel.Cell cell = getCell(row, getClassColumn(classIndex));
        cell.setCellValue(subjectTitle);
        cell.setCellStyle(SUBJECTS_OFFSET.getStyle(timetableSheet));
    }

    private static void setLesson(Sheet timetableSheet, int day, int lesson) {
        Row row = getRow(timetableSheet, getLessonRow(day, lesson));
        org.apache.poi.ss.usermodel.Cell cell = getCell(row, LESSONS_OFFSET.column);
        cell.setCellValue("" + lesson + ".");
        cell.setCellStyle(LESSONS_OFFSET.getStyle(timetableSheet));
    }

    private static void setClass(Sheet timetableSheet, int classIndex, String title) {
        Row row = getRow(timetableSheet, CLASSES_OFFSET.row);
        org.apache.poi.ss.usermodel.Cell cell = getCell(row, getClassColumn(classIndex));
        cell.setCellValue(title);
        cell.setCellStyle(CLASSES_OFFSET.getStyle(timetableSheet));
    }

    private static void setDay(Sheet timetableSheet, int day) {
        String dayTitle = DAYS.get(day);
        int currentDayRow = getFirstDayRow(day);
        Row row = getRow(timetableSheet, currentDayRow);
        org.apache.poi.ss.usermodel.Cell cell = getCell(row, DAYS_OFFSET.column);
        cell.setCellValue(dayTitle);
        setStyleForDay(timetableSheet, day);
        timetableSheet.addMergedRegion(new CellRangeAddress(currentDayRow,
                                                            currentDayRow + LESSONS,
                                                            DAYS_OFFSET.column,
                                                            DAYS_OFFSET.column));
    }

    private static void setStyleForDay(Sheet timetableSheet, int day) {
        CellStyle style = DAYS_OFFSET.getStyle(timetableSheet);
        style.setBorderTop(BorderStyle.THIN);
        rangeClosed(0, LESSONS).forEach(lesson -> {
            Row rowInMergedRegion = getRow(timetableSheet, getLessonRow(day, lesson));
            org.apache.poi.ss.usermodel.Cell cellInMergedRegion = getCell(rowInMergedRegion, DAYS_OFFSET.column);
            cellInMergedRegion.setCellStyle(style);
        });
    }

    private static int getDayOffset(int day) {
        return day * (LESSONS + 1);
    }

    private static int getFirstDayRow(int day) {
        return getDayOffset(day) + DAYS_OFFSET.row;
    }

    private static int getFirstLessonRow(int day) {
        return getDayOffset(day) + LESSONS_OFFSET.row;
    }

    private static int getLessonRow(int day, int lesson) {
        return getFirstLessonRow(day) + lesson;
    }

    private static int getClassColumn(int classIndex) {
        return CLASSES_OFFSET.column + classIndex;
    }

    private static String getSubjectTitle(int day, int lesson, String classId, Map<String, Subject> subjectsCache,
                                          List<Cell> cells) {
        return cells.stream()
                    .filter(cell -> cell.day == day && cell.lesson == lesson && Objects.equals(cell.clazz, classId))
                    .map(cell -> subjectsCache.getOrDefault(cell.subject, new Subject("NOT_FOUND", null, null)).title)
                    .distinct()
                    .collect(Collectors.joining(" / "));
    }
}
