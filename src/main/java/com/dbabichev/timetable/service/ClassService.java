package com.dbabichev.timetable.service;

import com.dbabichev.timetable.db.dao.ClassDao;
import com.dbabichev.timetable.model.Clazz;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 12:44 PM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class ClassService {

    private static final Logger LOG = LoggerFactory.getLogger(ClassService.class);

    private final ClassDao dao;

    public ClassService(ClassDao dao) {
        this.dao = dao;
    }

    public CompletableFuture<Clazz> addNewClass(String newClassTitle) {
        final Clazz newClass = new Clazz(newClassTitle);
        CompletableFuture<Clazz> clazzFuture = dao.addNewClass(newClass);
        clazzFuture.thenAccept(clazz -> LOG.info("New class [{}] has been added", clazz));
        return clazzFuture;
    }

    public CompletableFuture<Clazz> updateClass(String id, String title) {
        Clazz existingClass = new Clazz(id, title);
        CompletableFuture<Clazz> clazzFuture = dao.updateClass(existingClass);
        clazzFuture.thenAccept(clazz -> LOG.info("Class [{}] has been updated", clazz));
        return clazzFuture;
    }

    public CompletableFuture<Optional<Clazz>> getClass(String classId) {
        CompletableFuture<Clazz> clazzFuture = dao.getClass(classId);
        return clazzFuture.thenApply(clazz -> {
            LOG.info("Class [{}] has been got by id: [{}]", clazz, classId);
            return Optional.ofNullable(clazz);
        });
    }

    public CompletableFuture<List<Clazz>> getClasses() {
        CompletableFuture<List<Clazz>> classesFuture = dao.getClasses();
        classesFuture.thenAccept(classes -> LOG.info("All classes have been got"));
        return classesFuture;
    }

    public CompletableFuture<Boolean> deleteClass(String classId) {
        CompletableFuture<Boolean> deleteFuture = dao.deleteClass(classId);
        deleteFuture.thenAccept(isRemoved -> {
            if (isRemoved)
                LOG.info("Class with id: [{}] has been removed. Removal was acknowledged.", classId);
            else
                LOG.info("Class with id: [{}] has not been removed. Removal was not acknowledged.", classId);
        });
        return deleteFuture;
    }
}
