package com.dbabichev.timetable.service;

import com.dbabichev.timetable.db.dao.TeacherDao;
import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.model.Teacher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 12:44 PM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TeacherService {

    private static final Logger LOG = LoggerFactory.getLogger(TeacherService.class);

    private final TeacherDao teacherDao;

    public TeacherService(TeacherDao teacherDao) {
        this.teacherDao = teacherDao;
    }

    public CompletableFuture<Teacher> addNewTeacher(String newTeacherName, Boolean isManager, String managedClassId) {
        final Teacher newTeacher = new Teacher(newTeacherName, isManager, new Clazz(managedClassId, null));
        CompletableFuture<Teacher> teacherFuture = teacherDao.addNewTeacher(newTeacher);
        teacherFuture.thenAccept(teacher -> LOG.info("New teacher [{}] has been added", teacher));
        return teacherFuture;
    }

    public CompletableFuture<Teacher> updateTeacher(String id, String teacherName, Boolean isManager, String managedClassId) {
        final Teacher existingTeacher = new Teacher(id, teacherName, isManager, new Clazz(managedClassId, null));
        CompletableFuture<Teacher> teacherFuture = teacherDao.updateTeacher(existingTeacher);
        teacherFuture.thenAccept(teacher -> LOG.info("Teacher [{}] has been updated", teacher));
        return teacherFuture;
    }

    public CompletableFuture<Optional<Teacher>> getTeacher(String teacherId) {
        CompletableFuture<Teacher> teacherFuture = teacherDao.getTeacher(teacherId);
        return teacherFuture.thenApply(teacher -> {
            LOG.info("Teacher [{}] has been got by id: [{}]", teacher, teacherId);
            return Optional.ofNullable(teacher);
        });
    }

    public CompletableFuture<List<Teacher>> getTeachers() {
        CompletableFuture<List<Teacher>> teachersFuture = teacherDao.getTeachers();
        teachersFuture.thenAccept(teachers -> LOG.info("All teachers have been got"));
        return teachersFuture;
    }

    public CompletableFuture<Boolean> deleteTeacher(String teacherId) {
        CompletableFuture<Boolean> deleteFuture = teacherDao.deleteTeacher(teacherId);
        deleteFuture.thenAccept(isRemoved -> {
            if (isRemoved)
                LOG.info("Teacher with id: [{}] has been removed. Removal was acknowledged.", teacherId);
            else
                LOG.info("Teacher with id: [{}] has not been removed. Removal was not acknowledged.", teacherId);
        });
        return deleteFuture;
    }
}
