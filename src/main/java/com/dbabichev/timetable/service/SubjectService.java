package com.dbabichev.timetable.service;

import com.dbabichev.timetable.db.dao.SubjectDao;
import com.dbabichev.timetable.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/15/2016
 * Time: 12:44 PM
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class SubjectService {

    private static final Logger LOG = LoggerFactory.getLogger(SubjectService.class);

    private final SubjectDao subjectDao;

    public SubjectService(SubjectDao subjectDao) {
        this.subjectDao = subjectDao;
    }

    public CompletableFuture<Subject> addNewSubject(String title, String teacherId,
                                                    Map<String, Map<String, Object>> subjectLoad) {
        final Subject newSubject = new Subject(title, teacherObj(teacherId), getSubjectLoad(subjectLoad));
        CompletableFuture<Subject> subjectFuture = subjectDao.addNewSubject(newSubject);
        subjectFuture.thenAccept(subject -> LOG.info("New subject [{}] has been added", subject));
        return subjectFuture;
    }

    public CompletableFuture<Subject> updateSubject(String id, String title, String teacherId,
                                                    Map<String, Map<String, Object>> subjectLoad) {
        final Subject existingSubject = new Subject(id, title, teacherObj(teacherId), getSubjectLoad(subjectLoad));
        CompletableFuture<Subject> teacherFuture = subjectDao.updateSubject(existingSubject);
        teacherFuture.thenAccept(teacher -> LOG.info("Subject [{}] has been added", teacher));
        return teacherFuture;
    }

    public CompletableFuture<Optional<Subject>> getSubject(String subjectId) {
        CompletableFuture<Subject> subjectFuture = subjectDao.getSubject(subjectId);
        return subjectFuture.thenApply(subject -> {
            LOG.info("Subject [{}] has been got by id: [{}]", subject, subjectId);
            return Optional.ofNullable(subject);
        });
    }

    public CompletableFuture<List<Subject>> getSubjects() {
        CompletableFuture<List<Subject>> subjectsFuture = subjectDao.getSubjects();
        subjectsFuture.thenAccept(subjects -> LOG.info("All subjects have been got"));
        return subjectsFuture;
    }

    public CompletableFuture<Boolean> deleteSubject(String subjectId) {
        CompletableFuture<Boolean> deleteFuture = subjectDao.deleteSubject(subjectId);
        deleteFuture.thenAccept(isRemoved -> {
            if (isRemoved)
                LOG.info("Subject with id: [{}] has been removed. Removal was acknowledged.", subjectId);
            else
                LOG.info("Subject with id: [{}] has been removed, but removal was not acknowledged.", subjectId);
        });
        return deleteFuture;
    }

    private Teacher teacherObj(String teacherId) {
        return new Teacher(teacherId, null, false, null);
    }

    private Clazz classObj(String classId) {
        return new Clazz(classId, null);
    }

    private Map<Clazz, HoursSettings> getSubjectLoad(Map<String, Map<String, Object>> subjectLoad) {
        if (subjectLoad == null)
            return Collections.emptyMap();
        return subjectLoad.entrySet().stream()
                          .filter(entry -> !entry.getKey().equals("null") && entry.getValue() != null)
                          .collect(Collectors.toMap(entry -> classObj(entry.getKey()), entry -> new HoursSettings(entry.getValue())));
    }

}
