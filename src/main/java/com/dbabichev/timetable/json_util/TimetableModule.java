package com.dbabichev.timetable.json_util;

import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.model.TimetableWeek;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/27/2016
 * Time: 1:48 PM
 */
public class TimetableModule extends SimpleModule {

    public static final MapType timetableMapType = TypeFactory.defaultInstance().constructMapType(HashMap.class, Clazz.class, TimetableWeek.class);

    @Override
    public String getModuleName() {
        return "TimetableModule for serializing Map<Clazz, TimetableWeek> objects";
    }

    @Override
    public Version version() {
        return new Version(1, 0, 0, "", "com.dbabichev", "timetable");
    }

    @Override
    public void setupModule(SetupContext context) {
        super.setupModule(context);
        SimpleSerializers serializers = new SimpleSerializers();
        serializers.addSerializer(Clazz.class, new ClassSerializer());
        context.addKeySerializers(serializers);
    }
}
