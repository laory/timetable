package com.dbabichev.timetable.json_util;

import com.dbabichev.timetable.model.HoursSettings;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 1/20/2017
 * Time: 1:31 PM
 */
public class HoursSettingsSerializer extends JsonSerializer<HoursSettings> {

    @Override
    public void serialize(HoursSettings value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeObject(value.toMap());
    }
}
