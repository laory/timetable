package com.dbabichev.timetable.json_util;

import com.dbabichev.timetable.model.Clazz;
import com.dbabichev.timetable.model.TimetableWeek;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/27/2016
 * Time: 1:44 PM
 */
public class ObjectMapperFactory {

    public static ObjectMapper getTimetableObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        Jdk8Module jdk8Module = new Jdk8Module();
        TimetableModule timetableModule = new TimetableModule();
        objectMapper.registerModule(jdk8Module);
        objectMapper.registerModule(timetableModule);
        objectMapper.writerFor(TimetableModule.timetableMapType);
        return objectMapper;
    }
}
