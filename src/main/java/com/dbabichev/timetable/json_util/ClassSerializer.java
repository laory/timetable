package com.dbabichev.timetable.json_util;

import com.dbabichev.timetable.model.Clazz;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/27/2016
 * Time: 1:43 PM
 */
public class ClassSerializer extends JsonSerializer<Clazz> {

    @Override
    public void serialize(Clazz value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeFieldName(value.id);
    }
}
