/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp')
.config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
            when('/', {
                templateUrl: 'views/timetable.html',
                controller: "timetableController"
            }).
            when('/teachers', {
                templateUrl: 'views/teachers.html',
                controller: "teacherController"
            }).
            when('/classes', {
                templateUrl: 'views/classes.html',
                controller: "classController"
            }).
            when('/classes/:year/', {
                templateUrl: 'views/classes.html',
                controller: "classController"
            }).
            when('/subjects', {
                templateUrl: 'views/subjects.html',
                controller: "subjectController"
            }).
            when('/config', {
                templateUrl: 'views/config.html',
                controller: "configController"
            }).
            when('/timetable', {
                redirectTo: "/"
            }).
            otherwise('/');
    }
])
.run(["$rootScope", '$location', function($rootScope, $location) {
    $rootScope.tabClass = function(tabs) {
        var isActive = _.contains(tabs, $location.path());
        if (isActive) {
            return "active";
        }
        return "";
    }
}]);

function is(val) {
    return val !== undefined && val !== null;
}

function sortClasses(classes) {
    return _.sortBy(
        _.sortBy(classes, item => /\D/g.test(item.title) ? item.title.match(/\D/g).join() : "0"),
        item => /\d+/g.test(item.title) ? parseInt(item.title.match(/\d+/g).join()) : item.title
    )
}

function sortTeachers(teachers) {
    return teachers.sort((teacher1, teacher2) => localeSensitiveComparator(teacher1.name, teacher2.name));
}

function sortSubjects(subjects) {
    return subjects.sort((subject1, subject2) => localeSensitiveComparator(subject1.title, subject2.title));
}

function sortSubjectsByTeacherName(subjects, teachers) {
    return subjects.sort((subject1, subject2) => {
        var teacher1 = _.find(teachers, teacher => teacher.id === subject1.teacher) || {};
        var teacher2 = _.find(teachers, teacher => teacher.id === subject2.teacher) || {};
        return localeSensitiveComparator(teacher1.name, teacher2.name);
    });
}

function localeSensitiveComparator(v1, v2) {
    // Compare strings alphabetically, taking locale into account
    var a = is(v1) ? "" + v1 : "";
    var b = is(v2) ? "" + v2 : "";
    return a.toLowerCase().localeCompare(b.toLowerCase());
}

Array.prototype.mergeSort = function(compare) {

    // define default comparison function if none is defined
    if (compare == undefined || compare == null) {
        compare = (a, b) => {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        };
    };

    function merge(a, aux, lo, mid, hi) {
        function lessThan(num1, num2) {
            return compare(num1, num2) < 0;
        }

        // copy to aux[]
        for (var k = lo; k <= hi; k++) {
            aux[k] = a[k];
        }

        // merge back to a[]
        var i = lo, j = mid + 1;
        for (var k = lo; k <= hi; k++) {
            if      (i > mid)                   a[k] = aux[j++];
            else if (j > hi)                    a[k] = aux[i++];
            else if (lessThan(aux[j], aux[i]))  a[k] = aux[j++];
            else                                a[k] = aux[i++];
        }
    }

    function sort(a, aux, lo, hi) {
        if (hi <= lo) return;
        var mid = lo + Math.floor((hi - lo) / 2);
        sort(a, aux, lo, mid);
        sort(a, aux, mid + 1, hi);
        merge(a, aux, lo, mid, hi);
    }

    var length = this.length;
    if (length > 1) sort(this, new Array(length), 0, length - 1);

    return this;
};

function extractTitlesAndSort(items, propertyToExtract) {
    return _.chain(items)
                .pluck(propertyToExtract)
                .unique()
                .value()
                .sort(localeSensitiveComparator);
}

function buildException(e) {
    var message;
    if (e.exception === "ForeignKeyConstraintViolation") {
        var entities = {classes: "Класи", cells: "Розклад", subjects: "Предмети", teachers: "Вчителі"};
        var dependents = _.chain(e.dependents)
                                .keys()
                                .map(key => entities[key])
                                .value()
                                .join(", ");
        message = "Неможливо видалити об'єкт, що використовується у розділах: " + dependents;
    } else if (e.exception === "IntegrityConstraintViolation") {
        if (e.property === "subjectLoad") {
            message = "Неможливо встановити навантаження менше ніж вже використано у розкладі";
        } else if (e.property === "isDividable") {
            message = "Неможливо відключити поділ предмету для класу, що вже використовується у розкладі";
        }
    }
    return {
        message: is(message) ? message : e.message
    };
}