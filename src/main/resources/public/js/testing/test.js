/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory("test",
        ["teacherFactory", "subjectFactory", function(TeacherFactory, SubjectFactory) {

    test(false, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var teacher2 = TeacherFactory.Teacher("Sveta");
        return teacher1.equals(teacher2);
    }, "Different Teachers are not equal");

    test(true, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var teacher3 = TeacherFactory.Teacher("Vova");
        return teacher1.equals(teacher3);
    }, "Different Teachers with the same name are not equal");

    test(true, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        return teacher1.equals(teacher1);
    }, "Teacher equals to itself");

    test(true, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var teacher2 = TeacherFactory.TeacherWithId(teacher1.id, teacher1.name);
        return teacher1.equals(teacher2);
    }, "The same instances of a Teacher are equals");

    test(false, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var subject1 = SubjectFactory.Subject("Music", teacher1, {"1": 1, "2": 2, "3": 5});
        var subject2 = SubjectFactory.Subject("Painting", teacher1, {"1": 1, "2": 2, "3": 5});
        return subject1.equals(subject2);
    }, "Different subjects led by the same teacher are not equal");

    test(false, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var teacher2 = TeacherFactory.Teacher("Sveta");
        var subject2 = SubjectFactory.Subject("Music", teacher1, {"1": 1, "2": 2, "3": 5});
        var subject3 = SubjectFactory.Subject("Music", teacher2, {"1": 1, "2": 2, "3": 5});
        return subject2.equals(subject3);
    }, "The same subjects led by different teachers are not equal");

    test(false, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var teacher2 = TeacherFactory.Teacher("Sveta");
        var subject2 = SubjectFactory.Subject("Music", teacher1, {"1": 1, "2": 2, "3": 5});
        var subject3 = SubjectFactory.Subject("Painting", teacher2, {"1": 1, "2": 2});
        return subject2.equals(subject3);
    }, "Different subjects led by different teachers are not equal");

    test(true, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var subject1 = SubjectFactory.Subject("Music", teacher1, {"1": 1, "2": 2, "3": 5});
        return subject1.equals(subject1);
    }, "Subject is equal to itself");

    test(true, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var subject1 = SubjectFactory.Subject("Music", teacher1, {"1": 1, "2": 2, "3": 5});
        var subject2 = SubjectFactory.SubjectWithId(subject1.id, subject1.title, subject1.teacher, subject1.subjectLoad);
        return subject1.equals(subject2);
    }, "Two instances of the same subjects are equal");

    test(false, function() {
        var teacher1 = TeacherFactory.Teacher("Vova");
        var subject1 = SubjectFactory.Subject("Music", teacher1, {"1": 1, "2": 2, "3": 5});
        var subject2 = SubjectFactory.Subject("Music", teacher1, {"1": 1, "2": 2});
        return subject1.equals(subject2);
    }, "The same subjects led by the same teacher with different hoursPerClass are not equal");

    function test(expected, testExpr, testName) {
        if (expected !== testExpr()) throw new Error("Test: '" + testName + "' failed!");
        else console.log("Test: '" + testName + "' passed!")
    }

    return {};
}]);