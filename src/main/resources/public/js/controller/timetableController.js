/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').controller('timetableController',
        ["$scope", "$q", "webSocket", "classDao", "teacherDao", "subjectDao", "timetableDao", "configDao", "IDLE", "OPEN",
            "LOCKED", "MAX_DIVISIONS", "subjectFactory", "test",
        function TimetableController($scope, $q, webSocket, ClassDao, TeacherDao, SubjectDao, TimetableDao, ConfigDao,
            IDLE, OPEN, LOCKED, MAX_DIVISIONS, SubjectFactory, Test) {

    $scope.IDLE = IDLE;
    $scope.OPEN = OPEN;
    $scope.LOCKED = LOCKED;
    $scope.MAX_DIVISIONS = MAX_DIVISIONS;

    $scope.weekDays = ["Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя"];
    $scope.workingDays = _.range(5);
    $scope.lessons = _.range(8);

    $scope.$on("$destroy", function tearDown() {
        webSocket.unsubscribe("timetableModification", $scope.$id);
        webSocket.unsubscribe("subjectModification", $scope.$id);
        webSocket.unsubscribe("classModification", $scope.$id);
        webSocket.unsubscribe("teacherModification", $scope.$id);
    });

    $scope.teacherById = function(id) {
        return _.find($scope.teachers, function(teacher) {return teacher.id == id;});
    };

    $scope.classById = function(id) {
        return _.find($scope.classes, function(clazz) {return clazz.id == id;});
    };

    $scope.subjectById = function(id) {
        return _.find($scope.subjects, function(subject) {return subject.id == id;});
    };

    $scope.exportTimetable = () => {
        window.open("/export/timetable", "_blank");
    };

    $scope.exportTeacherTimetable = () => {
        window.open("/export/teacher_timetable", "_blank");
    };

    refreshTimetable();
    handleUpdates();

    function refreshTimetable() {
        $q.all([
            SubjectDao.getSubjects(),
            TeacherDao.getTeachers(),
            ClassDao.getClasses(),
            TimetableDao.getCells()
        ])
        .then(results => {
            $scope.teachers = sortTeachers(results[1]);
            $scope.subjects = results[0];
            $scope.classes = sortClasses(results[2]);
            $scope.settings = ConfigDao.getSettings();

            var cells = results[3];
            $scope.timetable = TimetableDao.generateTimetable($scope.classes, $scope.workingDays, $scope.lessons, cells);
            mergeTimetableWithCells($scope.timetable, cells);

            refreshSubjectBar();

            console.log($scope.timetable);
        });
    }

    function mergeTimetableWithCells(timetable, cells) {
        _.each(cells, cell => {
            var subject = $scope.subjectById(cell.subject);
            if (is(subject)) {
                var isCellMerged = timetable.mergeCell(cell, subject);
                if (isCellMerged) {
                    subject.increaseBusyHours(cell.clazz);
                }
            } else {
                console.error("Unable to find a subject by id", cell.subject, $scope.subjects);
            }
        });
    }

    function deleteCell(timetable, cell) {
        var isCellDeleted = timetable.deleteCell(cell);
        var subject = $scope.subjectById(cell.subject);
        if (is(subject) && isCellDeleted) {
            subject.decreaseBusyHours(cell.clazz);
        } else {
            console.error("Busy hours were not changed", cell, timetable, $scope.subjects);
        }
    }

    function handleUpdates() {
        webSocket.subscribe("timetableModification", $scope.$id, response => {
            $scope.$apply(() => {
                switch (response.operation) {
                    case "updateCell":
                        mergeTimetableWithCells($scope.timetable, [response]);
                        console.log("Updated cell", response);
                        break;
                    case "deleteCell":
                        deleteCell($scope.timetable, response);
                        console.log("Deleted cell", response);
                        break;
                    default:
                        console.log("timetableModification - default handler", response);
                }
                analyzeTimetable(getActiveSubject());
            });
        });
        webSocket.subscribe("subjectModification", $scope.$id, response => {
            var subject = JSON.parse(response.subject);
            var index = _.findIndex($scope.subjects, subj => {
                return subj.id === subject.id;
            });
            $scope.$apply(() => {
                switch (response.operation) {
                    case "updateSubject":
                        var updatedSubject = SubjectFactory.wrap(subject);
                        if (index == -1) {
                            $scope.subjects.push(updatedSubject);
                        } else {
                            $scope.subjects.splice(
                                index,
                                1,
                                _.extend(updatedSubject, {
                                    busyHoursPerClass: $scope.subjects[index].busyHoursPerClass
                                })
                            );
                            $scope.subjects[index].refreshFreeHoursPerClass();
                        }
                        console.log("Updated subject", response);
                        break;
                    case "deleteSubject":
                        $scope.subjects.splice(index, 1);
                        console.log("Deleted subject", response);
                        break;
                    default:
                        console.log("subjectModification - default handler", response);
                }
                refreshSubjectBar();
                analyzeTimetable(getActiveSubject());
                // TODO: update teacher names in timetable
            });
        });
        webSocket.subscribe("classModification", $scope.$id, response => {
            var clazz = JSON.parse(response.clazz);
            var index = _.findIndex($scope.classes, cls => {
                return cls.id === clazz.id;
            });
            $scope.$apply(() => {
                switch (response.operation) {
                    case "updateClass":
                        if (index == -1) {
                            $scope.classes.push(clazz);
                        } else {
                            _.extend($scope.classes[index], clazz);
                        }
                        console.log("Updated class", response);
                        break;
                    case "deleteClass":
                        $scope.classes.splice(index, 1);
                        console.log("Deleted class", response);
                        break;
                    default:
                        console.log("classModification - default handler", response);
                }
                refreshSortedClasses();
                analyzeTimetable(getActiveSubject());
            });
        });
        webSocket.subscribe("teacherModification", $scope.$id, response => {
            var teacher = JSON.parse(response.teacher);
            var index = _.findIndex($scope.teachers, tchr => {
                return tchr.id === teacher.id;
            });
            $scope.$apply(() => {
                switch (response.operation) {
                    case "updateTeacher":
                        if (index == -1) {
                            $scope.teachers.push(teacher);
                        } else {
                            _.extend($scope.teachers[index], teacher);
                        }
                        console.log("Updated teacher", response);
                        break;
                    case "deleteTeacher":
                        $scope.teachers.splice(index, 1);
                        console.log("Deleted teacher", response);
                        break;
                    default:
                        console.log("teacherModification - default handler", response);
                }
                refreshSubjectBar();
                analyzeTimetable(getActiveSubject());
            });
        });
    }

    function refreshSubjectBar() {
        refreshSortedSubjects();
        $scope.subjects = sortSubjectsByTeacherName($scope.subjects, $scope.teachers);
        _.each($scope.subjects, subject => {
            // $scope.classes are supposed to be sorted
            var sortedHoursPerClass = _.reduce($scope.classes, function(collector, clazz) {
                if (is(subject.subjectLoad[clazz.id])) {
                    collector.push({clazz: clazz, hours: subject.subjectLoad[clazz.id].hours, dividable: subject.subjectLoad[clazz.id].dividable});
                }
                return collector;
            }, []);
            subject.sortedHoursPerClass = sortedHoursPerClass;
        });
        $scope.subjectBar = _.groupBy($scope.subjects, "title");
    }

    function refreshSortedSubjects() {
        $scope.sortedSubjects = extractTitlesAndSort($scope.subjects, "title");
    }

    $scope.activate = function($event, subject) {
        $event.preventDefault();
        activateSubject(subject);
        analyzeTimetable(subject);
        console.log($scope.timetable)
    };

    $scope.addSubject = function(clazz, day, lesson, division) {
        var activeSubject = getActiveSubject();
        if (activeSubject === undefined) return;
        var timetableCellTuple = $scope.timetable.cell(clazz, day, lesson);
        var timetableCell = timetableCellTuple[division];
        if (is(timetableCell) && timetableCell.status === OPEN) {
            webSocket.sendMessage({
                routingKey  :   "timetableModification",
                operation   :   "updateCell",
                clazz       :   clazz,
                day         :   day,
                lesson      :   lesson,
                subject     :   activeSubject.id,
                division    :   division
            });
        }
    };

    $scope.removeSubject = function($event, cell, clazz, day, lesson, division) {
        $event.stopPropagation();
        var subjId = cell.subject;
        webSocket.sendMessage({
            routingKey  :   "timetableModification",
            operation   :   "deleteCell",
            clazz       :   clazz,
            day         :   day,
            lesson      :   lesson,
            subject     :   subjId,
            division    :   division
        });
    };

    function activateSubject(subject) {
        var currState = subject.active;
        $scope.subjects.forEach(function(item) {item.active = false;});
        subject.active = !currState;
        prepareTimetable(subject);
    }

    function prepareTimetable(subject) {
        var activeDividableClasses = [];
        if (subject.active) {
            activeDividableClasses = subject.getDividableClasses();
        }
        $scope.timetable.toggleDivisions(activeDividableClasses);
    }

    function getActiveSubject() {
        return _.find($scope.subjects, function(item) {return item.active === true;});
    }

    function analyzeTimetable(subject) {
        resetTimetableStatuses();
        if (subject !== undefined && subject.active === true) {
            refreshTimetableByCell(applicableForClass(subject));
            refreshTimetableByCell(enoughLessonsForClass(subject));
            refreshTimetableByDaySection(singleSubjectPerDayPerClass(subject));
            refreshTimetableByCell(singleTeacherPerDayAndLesson(subject));
            refreshTimetableByCell(matchingHoursForDivisions(subject));
            refreshTimetableByCell(setIdleToSetUpSubjects(subject));
        }
    }

    function resetTimetableStatuses() {
        refreshTimetableByCell(function(clazz, day, lesson, timetableCellTuple) {
            _.each(timetableCellTuple, timetableCell => {
                timetableCell.reset();
            });
        });
    }

    function applicableForClass(subject) {
        return function(clazz, day, lesson, timetableCellTuple) {
            _.each(timetableCellTuple, timetableCell => {
                if (subject.subjectLoad[clazz] !== undefined || !$scope.settings.get("applicableForClass")) {
                    timetableCell.open();
                } else {
                    timetableCell.lock("Предмет: \"" + subject.title + "\" не читається для учнів \"" + $scope.classById(clazz).title + "\" класу");
                }
            });
        };
    }

    function singleSubjectPerDayPerClass(subject) {
        return function(clazz, day, lessonLines) {
            var foundCellTuple = _.find(
                lessonLines,
                function(cellTuple, lesson) {
                    var foundIndex = _.findIndex(cellTuple, cell => {
                        return cell.subject !== null && !$scope.teacherById(subject.teacher).isManager && $scope.subjectById(cell.subject).title === subject.title;
                    });
                    return foundIndex !== -1;
                }
            );
            _.each(lessonLines, function(cellTuple) {
                _.each(cellTuple, targetCell => {
                    if (foundCellTuple === undefined || !$scope.settings.get("singleSubjectPerDayPerClass")) {
                        targetCell.open();
                    } else {
                        targetCell.lock("Предмет: \"" + subject.title + "\" не повинен повторюватися декілька разів в один день");
                    }
                });
            });
        };
    }

    function singleTeacherPerDayAndLesson(subject) {
        return function(clazz, day, lesson, timetableCellTuple) {
            _.each(timetableCellTuple, (timetableCell, division) => {
                var timetableCellSubject = $scope.subjectById(timetableCell.subject);
                if (timetableCellSubject !== undefined && subject.teacher === timetableCellSubject.teacher) {
                    _.each($scope.timetable.data, function(timetableLine, curClass) {
                        var cellTuple = timetableLine[day][lesson];
                        _.each(cellTuple, targetCell => {
                            targetCell.lock("Вчитель: \"" + $scope.teacherById(subject.teacher).name + "\" вже проводить предмет: \"" +
                                timetableCellSubject.title + "\" (" + lesson + " уроком) для учнів \"" +
                                $scope.classById(clazz).title + "\" класу");
                        });
                    });
                };
            });
        };
    }

    function matchingHoursForDivisions(subject) {
        var dividableClasses = subject.getDividableClasses();
        return (clazz, day, lesson, timetableCellTuple) => {
            if (_.contains(dividableClasses, clazz)) {
                var subjectPerDivision = _.chain(timetableCellTuple)
                    .filter(cell => is(cell.subjectObj))
                    .map(cell => cell.subjectObj)
                    .value();
                var nonDividableSubjects = _.chain(subjectPerDivision)
                    .filter(subjectObj => !subjectObj.subjectLoad[clazz].dividable)
                    .map(subjectObj => subjectObj.title)
                    .value();
                if (!_.isEmpty(nonDividableSubjects)) {
                    _.each(timetableCellTuple, targetCell => {
                        targetCell.lock('"' + $scope.classById(clazz).title + '" клас зможе поділитися на групи лише за умови, ' +
                            "якщо усі предмети, що проводять " + lesson + " урок поділяються на групи. Предмет: " +
                            nonDividableSubjects.join(", ") + " не поділяється");
                    });
                }
            } else if (timetableCellTuple.length > 1) {
                _.each(timetableCellTuple, targetCell => {
                    targetCell.lock(lesson + 'урок вже поділяється на групи');
                });
            }
        };
    }

    function enoughLessonsForClass(subject) {
        return function(clazz, day, lesson, timetableCellTuple) {
            _.each(timetableCellTuple, (timetableCell, i) => {
                if (subject.busyHoursPerClass[clazz] === undefined || subject.busyHoursPerClass[clazz] < subject.subjectLoad[clazz].hours
                        || !$scope.settings.get("enoughLessonsForClass")) {
                    timetableCell.open();
                } else {
                    timetableCell.lock("Вчитель: \"" + $scope.teacherById(subject.teacher).name + "\" вичерпав кількість годин (всього " +
                        subject.subjectLoad[clazz].hours + ") для учнів \"" + $scope.classById(clazz).title + "\" класу");
                }
            });
        };
    }

    function setIdleToSetUpSubjects(subject) {
        return function(clazz, day, lesson, timetableCellTuple) {
            _.each(timetableCellTuple, (timetableCell, i) => {
                if (timetableCell.subject !== null) {
                    timetableCell.reset();
                }
            });
        };
    }

    function refreshTimetableByCell(updateStatus) {
        _.each($scope.timetable.data, function(timetableLine, clazz) {
            _.each(timetableLine, function(lessonLines, day) {
                _.each(lessonLines, function(timetableCellTuple, lesson) {
//                    console.log("Class ", clazz, " day ", day, " lesson ", lesson);
                    updateStatus(clazz, day, lesson, timetableCellTuple);
                });
            });
        });
    }

    function refreshTimetableByDaySection(updateStatus) {
        _.each($scope.timetable.data, function(timetableLine, clazz) {
            _.each(timetableLine, function(lessonLines, day) {
//              console.log("Class ", clazz, " day ", day);
                updateStatus(clazz, day, lessonLines);
            });
        });
    }
}]);