/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').controller('subjectController',
        ["$scope", "$routeParams", "$q", "subjectFactory", "teacherDao", "classDao", "subjectDao",
        function SubjectController($scope, $routeParams, $q, SubjectFactory, TeacherDao, ClassDao, SubjectDao) {

    $scope.years = ["2016"];
    $scope.selectedYear = $routeParams.year || $scope.years[0];

    refreshSubjects();

    function refreshSubjects() {
        $scope.newSubject = SubjectFactory.Subject();
        $q.all([SubjectDao.getSubjects(), TeacherDao.getTeachers(), ClassDao.getClasses()])
            .then(results => {
                var sortedTeachers = sortTeachers(results[1]);
                var sortedSubjects = sortSubjectsByTeacherName(results[0], sortedTeachers)
                    .mergeSort((subject1, subject2) =>
                        localeSensitiveComparator(subject1.title, subject2.title)
                    );
                $scope.teachers = $scope.teachersToOptions(sortedTeachers);
                $scope.subjects = {"2016": sortedSubjects};
                $scope.classes = $scope.classesToOptions(sortClasses(results[2]));
                $scope.subjectTitles = _.pluck($scope.subjects[$scope.selectedYear], "title");
            });
    }

    $scope.updateSubject = function(subject, property, newValue) {
        subject[property] = newValue;
        SubjectDao.save(subject)
            .catch(response => {
                $scope.exception = buildException(response.data);
            })
            .finally(refreshSubjects);
    };

    $scope.addNewSubject = function() {
        SubjectDao.save($scope.newSubject).finally(refreshSubjects);
    };

    $scope.deleteSubject = function(subjectId) {
        SubjectDao.deleteSubject(subjectId)
            .catch(response => {
                $scope.exception = buildException(response.data);
            })
            .finally(refreshSubjects);
    };

    $scope.teachersToOptions = function(teachers) {
        return _.map(teachers, teacher => {
            return {val: teacher.id, title: teacher.name};
        });
    };

    $scope.classesToOptions = function(classes) {
        return _.map(classes, clazz => {
            return {val: clazz.id, title: clazz.title};
        });
    };

}]);