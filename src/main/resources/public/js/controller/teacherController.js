/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').controller('teacherController',
        ["$scope", "$routeParams", "$q", "teacherFactory", "teacherDao", "classDao",
        function ClassController($scope, $routeParams, $q, TeacherFactory, TeacherDao, ClassDao) {

    $scope.years = ["2016"];
    $scope.selectedYear = $routeParams.year || $scope.years[0];
    $scope.newTeacher = TeacherFactory.Teacher();

    refreshTeachers();

    function refreshTeachers() {
        $q.all([TeacherDao.getTeachers(), ClassDao.getClasses()])
            .then(results => {
                $scope.teachers = {"2016" : sortTeachers(results[0])};
                $scope.classes = sortClasses(results[1]);
                $scope.newTeacher = TeacherFactory.Teacher();
                $scope.freeClasses = _.reduce(
                    $scope.teachers[$scope.selectedYear].concat($scope.newTeacher),
                    (collector, teacher) => {
                        collector[teacher.id] = $scope.getFreeClasses(teacher);
                        return collector;
                    },
                    {}
                );
            });
    }

    $scope.updateTeacher = function(teacher, property, newValue) {
        teacher[property] = newValue;
        TeacherDao.save(teacher).finally(refreshTeachers);
    };

    $scope.addNewTeacher = function() {
        if ($scope.isNewTeacherValid()) {
            console.log($scope.newTeacher)
            TeacherDao.save($scope.newTeacher).finally(refreshTeachers);
        }
    };

    $scope.deleteTeacher = function(teacherId) {
        TeacherDao.deleteTeacher(teacherId)
            .catch(response => {
                $scope.exception = buildException(response.data);
            })
            .finally(refreshTeachers);
    };

    $scope.isNameValid = function(newName) {
        return newName !== null &&
            newName !== undefined &&
            newName !== '' &&
            !_.contains(_.pluck($scope.teachers[$scope.selectedYear], "name"), newName);
    };

    $scope.hasNoClass = function() {
        return $scope.newManagedClass === null || $scope.newManagedClass === undefined;
    };

    $scope.isManagerValid = function(value) {
        return true === value || false === value;
    };

    $scope.isManagedClassValid = function(value) {
        return  value !== undefined && value !== '';
    };

    $scope.isValid = function(name, isManager, managedClass) {
        return $scope.isNameValid(name) && $scope.isManagerValid(isManager) && $scope.isManagedClassValid(managedClass);
    };

    $scope.isNewTeacherValid = function() {
        var teacher = $scope.newTeacher;
        return $scope.isValid(teacher.name, teacher.isManager, teacher.managedClass);
    }

    $scope.getFreeClasses = function(teacher) {
        if (!is($scope.classes)) return [];
        var managedClasses = $scope.getManagedClassIds();
        return _.chain($scope.classes)
            .filter(clazz => {
                return !_.contains(managedClasses, clazz.id) ||
                        (is(teacher) && teacher.managedClass === clazz.id);
            })
            .map(clazz => {
                return {val: clazz.id, title: clazz.title};
            })
            .value();
    };

    $scope.getManagedClassIds = () => {
        if (!is($scope.teachers)) return [];
        return _.chain($scope.teachers[$scope.selectedYear])
            .map(teacher => {
                return teacher.managedClass;
            })
            .filter(managedClass => {
                return is(managedClass);
            })
            .value();
    };

}]);