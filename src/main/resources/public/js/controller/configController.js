/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').controller('configController',
        ["$scope", "$routeParams", "configDao", function ConfigController($scope, $routeParams, ConfigDao) {

    $scope.booleanSettings = [ // defaults
        {
            name: "singleSubjectPerDayPerClass",
            title: "Один предмет {value}може повторюватись в один день для одного класу. Виключення - керівний склад. Див. розділ 'Вчителі'.",
            value: true
        },
        {
            name: "applicableForClass",
            title: "Вчитель {value}може читати предмет для класу, що НЕ вказаний у 'Навантаженні'. Див. розділ 'Предмети'.",
            value: true
        },
        {
            name: "enoughLessonsForClass",
            title: "Вчитель {value}може читати предмет для класу більше годин ніж зазначено у 'Навантаженні'. Див. розділ 'Предмети'.",
            value: true
        }
    ];

    $scope.save = () => {
        var settings = _.reduce($scope.booleanSettings, (collector, setting) => {
            collector[setting.name] = setting.value;
            return collector;
        }, {});
        ConfigDao.save(settings);
        $scope.load();
    };

    $scope.load = () => {
        var settings = ConfigDao.getSettings();
        if (is(settings)) {
            _.each($scope.booleanSettings, setting => {
                if (is(settings[setting.name])) {
                    setting.value = settings[setting.name];
                }
            });
        }
    };

    $scope.showTitle = (title, value) => {
        return title.replace("{value}", value === false ? "" : "НЕ ");
    };

    $scope.load();
}]);
