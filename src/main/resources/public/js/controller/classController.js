/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').controller('classController',
        ["$scope", "$routeParams", "$q", "classFactory", "classDao", "teacherDao", function ClassController($scope, $routeParams,
        $q, ClassFactory, ClassDao, TeacherDao) {

    $scope.years = ["2016"];
    $scope.selectedYear = $routeParams.year || $scope.years[0];
    $scope.newClassHolder = ClassFactory.Class();

    refreshClasses();

    function refreshClasses() {
        $q.all([TeacherDao.getTeachers(), ClassDao.getClasses()])
            .then(results => {
                var teachers = results[0];
                var classes = results[1];
                _.each(teachers, teacher => {
                    if (is(teacher.managedClass)) {
                        var foundClass = _.find(classes, clazz => clazz.id === teacher.managedClass);
                        foundClass.teacher = teacher;
                    }
                });
                $scope.classes = {"2016" : sortClasses(classes)};
                $scope.newClassHolder = ClassFactory.Class();
            });
    }

    $scope.updateClass = function(clazz, property, newValue) {
        if ($scope.isValid(newValue)) {
            clazz[property] = newValue;
            ClassDao.save(clazz).finally(refreshClasses);
        }
    };

    $scope.addClass = function() {
        if ($scope.isValid($scope.newClassHolder.title)) {
            ClassDao.save($scope.newClassHolder).finally(refreshClasses);
        }
    };

    $scope.deleteClass = function(clazzId) {
        ClassDao.deleteClass(clazzId)
            .catch(response => {
                $scope.exception = buildException(response.data);
            })
            .finally(refreshClasses);
    };

    $scope.isValid = function(newClassTitle) {
        return newClassTitle !== null &&
            newClassTitle !== undefined &&
            newClassTitle !== '' &&
            !_.contains(_.pluck($scope.classes[$scope.selectedYear], "title"), newClassTitle);
    };

}]);