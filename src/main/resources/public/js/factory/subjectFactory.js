/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory("subjectFactory", function() {
    return {
        Subject: function(title, teacher, subjectLoad) {
            return {
                title: title,
                teacher: teacher,
                subjectLoad: subjectLoad,
                busyHoursPerClass: {},
                dividable: false,
                equals: equals
            };
        },
        SubjectWithId: function(id, title, teacher, subjectLoad) {
            return {
                id: id,
                title: title,
                teacher: teacher,
                subjectLoad: subjectLoad,
                busyHoursPerClass: {},
                dividable: false,
                equals: equals
            };
        },
        wrap: function(data) {
            var wrapper = {
                busyHoursPerClass: {},
                freeHoursPerClass: {},
                increaseBusyHours: function increaseBusyHours(clazz) {
                    if (is(this.busyHoursPerClass[clazz])) {
                        this.busyHoursPerClass[clazz]++;
                    } else {
                        this.busyHoursPerClass[clazz] = 1;
                    }
                    this.updateFreeHoursPerClass(clazz);
                },
                decreaseBusyHours: function decreaseBusyHours(clazz) {
                    if (is(this.busyHoursPerClass[clazz])) {
                        this.busyHoursPerClass[clazz]--;
                    } else {
                        this.busyHoursPerClass[clazz] = 0;
                    }
                    this.updateFreeHoursPerClass(clazz);
                },
                updateFreeHoursPerClass: function updateFreeHoursPerClass(clazz) {
                    this.freeHoursPerClass[clazz] = this.subjectLoad[clazz].hours - (this.busyHoursPerClass[clazz] || 0);
                },
                getDividableClasses: function getDividableClasses() {
                    return _.chain(this.subjectLoad)
                        .pick(hoursSettings => {
                            return hoursSettings.dividable;
                        })
                        .keys()
                        .value();
                },
                refreshFreeHoursPerClass: function refreshFreeHoursPerClass() {
                    _.each(this.subjectLoad, (hoursSettings, clazz) => {
                        this.updateFreeHoursPerClass(clazz);
                    });
                }
            };
            var subject = _.extend({}, data, wrapper);
            subject.refreshFreeHoursPerClass();
            return subject;
        }
    };

    function equals(otherSubject) {
        if (otherSubject === null || otherSubject === undefined) return false;
        if (this === otherSubject) return true;
        return this.id === otherSubject.id &&
            this.title === otherSubject.title &&
            this.teacher.equals(otherSubject.teacher) &&
            _.isEqual(this.subjectLoad, otherSubject.subjectLoad);
    }
});