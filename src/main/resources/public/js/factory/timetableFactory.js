/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp')
.factory("timetableFactory", function(IDLE, OPEN, LOCKED, MAX_DIVISIONS) {

    return {
        Timetable: function(classes, workingDays, lessons) {
            var timetable = {
                id: UUID(),
                data: {},
                line: line,
                daySection: daySection,
                cell: cell,
                mergeCell: mergeCell,
                deleteCell: deleteCell,
                isCellValid: isCellValid,
                toggleDivisions: toggleDivisions
            };
            _.map(classes, function(clazz) {
                var timetableLine = [];
                _.each(workingDays, function(day) {
                    var lessonLines = [];
                    _.each(lessons, function(lesson) {
                        lessonLines.push([Cell()]);
                    });
                    timetableLine.push(lessonLines);
                });
                timetable.data[clazz.id] = timetableLine;
            });
            return timetable;
        }
    };

    function line(clazz) {
        var data = this.data;
        return !_.isEmpty(data) && !_.isEmpty(data[clazz]) ? data[clazz] : {};
    }

    function daySection(clazz, day) {
        var line = this.line(clazz);
        return !_.isEmpty(line) && !_.isEmpty(line[day]) ? line[day] : {};
    }

    function cell(clazz, day, lesson) {
        var daySection = this.daySection(clazz, day);
        return !_.isEmpty(daySection) && !_.isEmpty(daySection[lesson]) ? daySection[lesson] : [];
    }

    function mergeCell(cell, subjectObj) {
        if (this.isCellValid(cell)) {
            var division = is(cell.division) ? cell.division : 0;
            var timetableCellTuple = this.cell(cell.clazz, cell.day, cell.lesson);
            while (division >= timetableCellTuple.length) {
                timetableCellTuple.push(Cell());
            }
            var timetableCell = timetableCellTuple[division];
            if (timetableCell.subject !== cell.subject) {
                timetableCell.setSubject(cell.subject, subjectObj);
                return true;
            }
        } else {
            console.error("Unable to set a cell", cell, this);
        }
        return false;
    }

    function deleteCell(cell) {
        if (this.isCellValid(cell)) {
            var division = is(cell.division) ? cell.division : 0;
            var timetableCell = this.cell(cell.clazz, cell.day, cell.lesson)[division];
            if (is(timetableCell) && is(timetableCell.subject)) {
                timetableCell.removeSubject();
                return true;
            }
        } else {
            console.error("Unable to delete a cell", cell, this);
        }
        return false;
    }

    function isCellValid(cell) {
        return is(cell) &&
            is(cell.subject) &&
            is(cell.clazz) &&
            is(cell.day) &&
            is(cell.lesson) &&
            !_.isEmpty(this.cell(cell.clazz, cell.day, cell.lesson));
    }

    function toggleDivisions(dividableClasses) {
        function getEmptyDivisionIndices(lessonTuple) {
            return _.chain(lessonTuple)
                .map((lesson, division) => {
                    return !is(lesson.subject) ? division : -1;
                })
                .filter(index => index > 0)
                .value();
        }

        function removeRedundantDivisions(lessonTuple) {
            var emptyDivisionIndices = getEmptyDivisionIndices(lessonTuple);
            _.each(emptyDivisionIndices, emptyDivisionIndex => {
                lessonTuple.splice(emptyDivisionIndex, 1);
            });
        }

        function addMissingDivisions(lessonTuple) {
            while (lessonTuple.length < MAX_DIVISIONS) {
                lessonTuple.push(Cell());
            }
        }

        _.each(this.data, (timetableLine, clazz) => {
            var isDividableClass = _.contains(dividableClasses, clazz);
            _.each(timetableLine, (lessons, day) => {
                _.each(lessons, (lessonTuple, lessonNumber) => {
                    if (!isDividableClass) {
                        removeRedundantDivisions(lessonTuple);
                    } else {
                        addMissingDivisions(lessonTuple);
                    }
                });
            });
        });
    }

    function Cell(subject) {
        return {
            subject: subject || null,
            status: IDLE,
            description: "",
            reset: function() {
                this.description = "";
                this.status = IDLE;
            },
            open: function() {
                if (this.status !== LOCKED) {
                    this.description = "";
                    this.status = OPEN;
                }
            },
            lock: function(description) {
                this.status = LOCKED;
                if (this.description.indexOf(description) === -1) {
                    this.description += ("\n" + description);
                }
            },
            removeSubject: function() {
                this.subject = null;
                this.subjectObj = null;
            },
            setSubject: function(subjectId, subjectObj) {
                this.subject = subjectId;
                if (is(subjectObj) && subjectId === subjectObj.id) {
                    this.subjectObj = subjectObj;
                }
            }
        };
    }
});