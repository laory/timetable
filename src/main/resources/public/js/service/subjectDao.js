/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory('subjectDao', ["$http", "subjectFactory", function($http, SubjectFactory) {

    function getSubjects() {
        return $http({
                method  :   "GET",
                url     :   "/subjects"
            })
            .then(function success(response) {
                return _.map(response.data, data => SubjectFactory.wrap(data));
            });
    }

    function getSubjectById(subjectId) {
        return $http({
                method  :   "GET",
                url     :   "/subjects/subject/" + subjectId
            }).then(function success(response) {
                return SubjectFactory.wrap(response.data);
            });
    }

    function save(subject) {
        var url = is(subject) && is(subject.id) ? "/subjects/subject/update" : "/subjects/subject";
        return $http({
                method  :   "PUT",
                url     :   url,
                data    :   subject
            }).then(function success(response) {
                return SubjectFactory.wrap(response.data);
            });
    }

    function deleteSubject(subjectId) {
        return $http({
                method  :   "DELETE",
                url     :   "/subjects/subject/" + subjectId
            }).then(function success(response) {
                return response.data;
            });
    }

    return {
        save: save,
        getSubjectById: getSubjectById,
        deleteSubject: deleteSubject,
        getSubjects: getSubjects
    };
}]);