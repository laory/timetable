/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory('widgetInitializer', function() {

    return {
        init: function(scope) {
            scope.READ = "READ";
            scope.WRITE = "WRITE";

            scope.isReadMode = function() {
                return scope.checkMode(scope.READ);
            };
            scope.isWriteMode = function() {
                return scope.checkMode(scope.WRITE);
            };
            scope.checkMode = function(mode) {
                return scope.mode === mode;
            };

            scope.enableReadMode = function() {
                scope.changeMode(scope.READ);
            };
            scope.enableWriteMode = function() {
                if (scope.options !== undefined) {
                    scope.newValue = scope.getSelectedOption();
                } else {
                    scope.newValue = scope.value;
                }
                scope.changeMode(scope.WRITE);
            };
            scope.changeMode = function(newMode) {
                if (scope.stateless) {
                    scope.mode = scope.WRITE;
                } else {
                    if (scope.isModeFixed()) {
                        scope.mode = scope.fixedMode;
                    } else {
                        scope.mode = newMode;
                    }
                }
            };
            scope.isModeFixed = function() {
                return scope.fixedMode !== undefined && scope.fixedMode !== null;
            };

            scope.save = function(newValue) {
                console.log(newValue)
                scope.callback({
                    index: scope.index,
                    newValue: newValue === undefined ? null : newValue
                });
                scope.enableReadMode();
            };

            scope.changeMode(scope.READ);

            scope.getSelectedOption = function() {
                return _.find(scope.options, function(option) {return scope.value === option.val;});
            };

            scope.isValid = function(value) {
                var valid = scope.validator({
                    newValue: value
                });
                return valid === undefined ? true : valid;
            };
        }
    };
});