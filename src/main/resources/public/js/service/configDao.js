/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory('configDao', [function() {

    function getSettings() {
        var settings = JSON.parse(window.localStorage.getItem("settings")) || {};
        settings.get = name => is(settings[name]) ? settings[name] : true;
        return settings;
    }

    function save(settings) {
        window.localStorage.setItem("settings", JSON.stringify(settings));
        console.log("Saved settings", settings);
    }

    return {
        save: save,
        getSettings: getSettings
    };
}]);