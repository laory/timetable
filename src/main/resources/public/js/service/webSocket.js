/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory('webSocket', function() {

    var ws = new WebSocket("ws://localhost:4567/timetable");

    ws.onopen = function() {
        console.log("WS connection is open");
    };

    ws.onmessage = function (evt) {
        var receivedMsg = JSON.parse(evt.data);
        console.log("Received message: ", receivedMsg)
        var appropriateHandlers = handlers[receivedMsg.routingKey];
        if (_.isEmpty(appropriateHandlers)) {
            console.log("Default handler");
        } else {
            _.each(appropriateHandlers, handler => {
                handler(receivedMsg);
            });
        }
    };

    ws.onclose = function() {
        console.log("WS connection is closed");
    };

    var handlers = {};

    return {
        sendMessage: message => {
            ws.send(JSON.stringify(message));
        },
        subscribe: (topic, handlerId, handler) => {
            if (!is(handlers[topic])) {
                handlers[topic] = {};
            }
            handlers[topic][handlerId] = handler;
            console.log("Handler '", handlerId, "' subscribed to topic '", topic, "'. All handlers", handlers);
        },
        unsubscribe: (topic, handlerId) => {
            if (is(handlers[topic])) {
                delete handlers[topic][handlerId];
                console.log("Handler '", handlerId, "' unsubscribed from topic '", topic, "'. All handlers", handlers);
            } else {
                console.log("Handler '", handlerId, "' was never subscribed to topic '", topic, "'. All handlers", handlers);
            }
        }
    };
});