/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory('teacherDao', ["$http", function($http) {

    function getTeachers() {
        return $http({
                method  :   "GET",
                url     :   "/teachers"
            })
            .then(function success(response) {
                return response.data;
            });
    }

    function getTeacherById(teacherId) {
        return $http({
                method  :   "GET",
                url     :   "/teachers/teacher/" + teacherId
            }).then(function success(response) {
                return response.data;
            });
    }

    function save(teacher) {
        var url = is(teacher) && is(teacher.id) && teacher.id !== "NEW" ? "/teachers/teacher/update" : "/teachers/teacher";
        return $http({
                method  :   "PUT",
                url     :   url,
                data    :   teacher
            }).then(function success(response) {
                return response.data;
            });
    }

    function deleteTeacher(teacherId) {
        return $http({
                method  :   "DELETE",
                url     :   "/teachers/teacher/" + teacherId
            }).then(function success(response) {
                return response.data;
            });
    }

    return {
        save: save,
        getTeacherById: getTeacherById,
        deleteTeacher: deleteTeacher,
        getTeachers: getTeachers
    };
}]);