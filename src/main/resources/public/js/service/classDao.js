/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').factory('classDao', ["$http", function($http) {

    function getClasses() {
        return $http({
                method  :   "GET",
                url     :   "/classes"
            })
            .then(function success(response) {
                return response.data;
            });
    }

    function getClassById(classId) {
        return $http({
                method  :   "GET",
                url     :   "/classes/class/" + classId
            }).then(function success(response) {
                return response.data;
            });
    }

    function save(clazz) {
        var url = is(clazz) && is(clazz.id) ? "/classes/class/update" : "/classes/class";
        return $http({
                method  :   "PUT",
                url     :   url,
                data    :   clazz
            }).then(function success(response) {
                return response.data;
            });
    }

    function deleteClass(classId) {
        return $http({
                method  :   "DELETE",
                url     :   "/classes/class/" + classId
            }).then(function success(response) {
                return response.data;
            });
    }

    return {
        save: save,
        getClassById: getClassById,
        deleteClass: deleteClass,
        getClasses: getClasses
    };
}]);