/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp')
.directive("hoursPerClass", function() {
    return {
        restrict: "E",
        templateUrl: "views/widget/composed/hoursPerClass.html",
        scope: {
            stateless: "<",
            fixedMode: "<",

            value: "<", // for stateful

            holder: "<", // for stateless
            getValueBy: "<", // for stateless

            classes: "<",
            callback: "&",
            validator: "&",
        },
        controller: function($scope, widgetInitializer) {
            widgetInitializer.init($scope);

            function initSettings() {
                // $scope.classes are supposed to be sorted
                $scope.displayValue = sortHoursPerClass($scope.classes, $scope.value, classOption => classOption.title);
                $scope.settings = sortHoursPerClass($scope.classes, $scope.value, classOption => classOption.val);
                if (_.isEmpty($scope.settings)) {
                    $scope.settings.push({classId: null, hours: null, dividable: false});
                }
            }

            function sortHoursPerClass(sortedClassOptions, subjectLoad, keyBuilder) {
                return _.reduce(sortedClassOptions, function(collector, classOption) {
                    if (is(subjectLoad[classOption.val])) {
                        collector.push({classId: keyBuilder(classOption), hours: subjectLoad[classOption.val].hours,
                            dividable: subjectLoad[classOption.val].dividable});
                    }
                    return collector;
                }, []);
            }

            $scope.prepareAndSave = function() {
                var newValue = _.reduce($scope.settings, function(collector, setting) {
                    var hours = parseInt(setting.hours);
                    var classId = setting.classId;
                    var dividable = setting.dividable;
                    if (is(classId) && classId !== "" && !isNaN(hours))
                        collector[classId] = {
                            hours: hours,
                            dividable, dividable
                        };
                    return collector;
                }, {});
                console.log(newValue)
                $scope.save(newValue);
            };

            $scope.addSetting = function() {
                $scope.settings.push({});
            };

            $scope.removeSetting = function(index) {
                $scope.settings.splice(index, 1);
            };

            $scope.isEmpty = function(val) {
                return _.isEmpty(val);
            };

            initSettings();
        }
    };
});
