/**
 * Copyright 2016 Dmytro Babichev
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('timetableApp').directive("editableText", ["widgetInitializer", function(widgetInitializer) {
    return {
        restrict: "E",
        templateUrl: "views/widget/editableText.html",
        scope: {
            placeholder: "<",

            stateless: "<",
            fixedMode: "<",

            value: "<", // for stateful
            typeahead: "<",

            holder: "<", // for stateless
            getValueBy: "<", // for stateless

            callback: "&",
            validator: "&"
        },
        link: function($scope, $element) {
            widgetInitializer.init($scope);
            if ($scope.stateless) {
                $scope.$watch("typeahead", changes => {
                    enableTypeahead();
                });
            }
            $scope.enableTextWriteMode = () => {
                $scope.enableWriteMode();
                enableTypeahead();
            };
            function enableTypeahead() {
                if (is($scope.typeahead)) {
                    var engine = new Bloodhound({
                        local: $scope.typeahead,
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        datumTokenizer: Bloodhound.tokenizers.whitespace
                    });
                    var input = $($element).find("input.typeahead");
                    input
                        .typeahead("destroy")
                        .typeahead(
                            {highlight: true, hint: true},
                            {source: engine}
                        )
                        .off("typeahead:select")
                        .on("typeahead:select", (e, suggestion) => {
                            input.val(suggestion).trigger("input");
                        });
                }
            }
        }
    };
}]);