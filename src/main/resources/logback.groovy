import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender

import static ch.qos.logback.classic.Level.ERROR
import static ch.qos.logback.classic.Level.INFO

appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-5level %logger{50}:%line [%thread] - %msg%n"
    }
}

appender("DB_LOG_FILE", FileAppender) {
    file = "log/database.log"
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-5level %logger{50}:%line [%thread] - %msg%n"
    }
}

appender("APPLICATION_LOG_FILE", FileAppender) {
    file = "log/application.log"
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-5level %logger{50}:%line [%thread] - %msg%n"
    }
}

root(INFO, ["STDOUT"])
logger("com.dbabichev.timetable.db", ERROR, ["DB_LOG_FILE", "STDOUT"], false)
logger("com.dbabichev.timetable", ERROR, ["APPLICATION_LOG_FILE"])